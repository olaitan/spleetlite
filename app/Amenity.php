<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    /**
    *The model associated with the Amenities table
    *
    */
    protected $table='amenities';
    public $timestamps=false;

    //Many to many relationship with space
    public function spaces()
    {
    	return $this->belongsToMany('App\Space','spaceamenities','amenityid','spaceid')->withPivot('number');
    }
}
