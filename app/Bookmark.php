<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    public $timestamps=false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'tenantid', 'spaceid',
    ];

    //One to many(inverse) relationship with tenant
    public function tenant()
    {
    	return $this->belongsTo('App\Tenant','tenantid');
    }

    //One to many(inverse) relationship with space
    public function space()
    {
    	return $this->belongsTo('App\Space','spaceid');
    }
}
