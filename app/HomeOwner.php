<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeOwner extends Model
{
    /**
    *The model associated with the HomeOwners table
    *
    */
    
    public $timestamps=false;

    //One to many relationship with spaces table
    public function spaces()
    {
    	return $this->hasMany('App\Space','homeownerid');
    }
    
}
