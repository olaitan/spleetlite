<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;

class TenantLoginController extends Controller
{

    public function __construct()
    {
      $this->middleware('guest:tenant', ['except' => ['logout']]);
    }
    
    public function showLoginForm()
    {
      return view('auth.tenant_login');
    }
    
    public function login(Request $request)
    {
      // Validate the form data
      $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:tenants',
            'password' => 'required|string|min:6',
        ]);
      if($validator->fails()){
         return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
      }

      // Attempt to log the user in
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        // if successful, then redirect to their intended location
        $user = Auth::user();
        return redirect($request->route);
      } 
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
