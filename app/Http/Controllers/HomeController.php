<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Space;
use App\AvailableLocation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$spaces = Space::where('notavailable','0')->orderByRaw('RAND()')->take(4)->get();
        $locations=AvailableLocation::orderBy('area')->get();
    	$data=array('spaces' =>$spaces,'locations'=>$locations);
        return view('index',$data);
    }

}
