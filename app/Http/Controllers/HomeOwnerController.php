<?php

namespace App\Http\Controllers;
use Mail;

use App\HomeOwner;
use App\PotentialHomeowner;
use App\Mail\SpacesRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeOwnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('homeowner');
    }

    public function register(Request $request)
    {
        //Run
        
        $potentialhomeowner=new PotentialHomeowner;
        $potentialhomeowner->name=$request->input('fullname');
        $potentialhomeowner->email=$request->input('email');
        $potentialhomeowner->mobile=$request->input('phone');
        $potentialhomeowner->address=$request->input('spaceAddress');
        $potentialhomeowner->time=$request->input('time');
        $potentialhomeowner->date=$request->input('date');
        $potentialhomeowner->photodirectory=$request->input('email');
        $potentialhomeowner->save();


        //return $this->html_email($request->input('email'),$potentialhomeowner->name,'Potential HomeOwner','Space Registering Application',$potentialhomeowner->address,$potentialhomeowner->mobile);

        Mail::to($potentialhomeowner->email)->send(new SpacesRegister($potentialhomeowner));
        Mail::to('iatoyebi@hotmail.co.uk')->send(new SpacesRegister($potentialhomeowner));

        return redirect()->back()->with('status','Request received successfully');
    }

    
}
