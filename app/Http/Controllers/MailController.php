<?php

namespace App\Http\Controllers;
use Mail;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function basic_email(){
      $data = array('name'=>"Spleet Ng");
   
      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('iamolaitanajibola@gmail.com', 'Spleet Tech')->subject
            ('Laravel Basic Testing Mail');
         $message->from('olaitan.adetayo@gmail.com','Spleet');
      });
      echo "Basic Email Sent. Check your inbox.";
   }

   public function html_email($email,$name,$usertype,$subject,$address,$mobile){
      $data = array('name'=>$name,'address'=>$address,'mobile'=>$mobile);
      Mail::send('mail', $data, function($message) {
         $message->to($email, $usertype)->subject
            ($subject);
         $message->from('olaitan.adetayo@gmail.com','SpleetTech');
      });
      echo "HTML Email Sent. Check your inbox.";
   }
   
   public function attachment_email(){
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
         $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
         $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
         $message->from('xyz@gmail.com','Virat Gandhi');
      });
      echo "Email Sent with attachment. Check your inbox.";
   }
}
