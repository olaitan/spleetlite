<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Space;
use App\SpaceBookingTransaction;
use App\SpaceContract;
use Illuminate\Support\Facades\Auth;

class PaystackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function verify($ref,$id)
    {
    	$result = array();
		//The parameter after verify/ is the transaction reference to be verified
		$client = new \GuzzleHttp\Client([
                'headers' => ['Authorization' =>'Bearer sk_test_10152f1b2ba77aa87defe1aefee9cd597a56a657']
            ]
        );
		$URI = 'https://api.paystack.co/transaction/verify/'.$ref;

		$request = $client->get($URI);
		$response = $request->getBody()->getContents();
		if ($request) {
		  $result = json_decode($response, true);
		}
		if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['data']['status'] === 'success')) {
			//Update the property as sold
			$space = Space::find($id);
			$space->notavailable = 1;
			$space->save();

			//create spacebookingtransaction
			$sbt=	new SpaceBookingTransaction;
			$sbt->tenantid=Auth::id();
			$sbt->bookingdate=date('Y-m-d H:i:s');;
			$sbt->paymentstatus='success';
			$sbt->paymentreference=$result['data']['reference'];
			$sbt->amount=$result['data']['amount'];
			$sbt->isautomaticallyrecurring=1;
			$sbt->spaceid=$id;
			$sbt->authorizationcode=$result['data']['authorization']['authorization_code'];

			$sbt->save();

			//create spacecontract

			$today = date("Y-m-d");
			$date=null;
			//check frequency of the space and add recurring
			if($space->paymentfrequency=="month"){
				$date = date('Y-m-d');
			}else{
				$date = date('Y-m-d');
			}
			
			$sc=new SpaceContract;
			$sc->startdate=$today;
			$sc->enddate=$date;
			$sc->spaceid=$id;
			$sc->tenantid=Auth::id();
			$sc->save();

			return view('welcome');
				//Perform necessary action
			}else{
			  echo "Transaction was unsuccessful";
			}
    
	}
}