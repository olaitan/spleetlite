<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Space;
use App\Location;
use App\SpaceRegister;
use App\AvailableLocation;

class SpaceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($spaceid)
    {
        //scrutinize the spaceid (not equals 0 and an integer)

        $space = Space::find($spaceid);
        $spaces = Space::where('notavailable','0')->whereNotIn('id',[$spaceid])->orderByRaw('RAND()')->take(4)->get();
        if ($space==null){
            //should redirect to the search and show similar to the space
            return "No such property";
        }else{
            //initialize and send the data to the view
            $data=array('space' =>$space,'similarspaces'=>$spaces);
           return view('space',$data); 
        }
 
    }
    public function spaces()
    {
        $spaces = Space::where('notavailable','0')->orderByRaw('RAND()')->paginate(5);
        $similar=0;
        $locations=AvailableLocation::orderBy('area')->get();
        return view('search', compact(['spaces','similar','locations']));
    }

    public function search(Request $request)
    {
        $tcoll=array();
        //Prepare the search variables
        if ($request->input('location')!='') {
            $lc = array('area','=',$request->input('location'));
            $tcoll[]=$lc;
        }
        if ($request->input('spacetype')!='') {
            $spacetype=($request->input('spacetype')=='Single Space')?1:2;
            $stype = array('spacetypeid','=',$spacetype);
            $tcoll[]=$stype;
            }
        if ($request->input('duration')!='') {
            $frequency=($request->input('duration')=='Less than a month')?'night':'month';
            $pf = array('paymentfrequency','=',$frequency);
            $tcoll[]=$pf;
        }
        if ($request->input('minprice')!='') {
            $minprice=$request->input('minprice');
            $minp = array('price','>=',$minprice);
            $tcoll[]=$minp;
        }
        if ($request->input('maxprice')!='') {
            $maxprice=$request->input('maxprice');
            $maxp = array('price','<=',$maxprice);
            $tcoll[]=$maxp;
        }
        if ($request->input('guard')!='') {
            $guard=($request->input('guard')=='Yes')?'1':'0';
            $gd = array('guard','=',$guard);
            $tcoll[]=$gd;
        }
        if ($request->input('parkingspace')!='') {
            $parkingspace=($request->input('parkingspace')=='Yes')?'1':'0';
            $parkspace = array('parkingspace','=',$parkingspace);
            $tcoll[]=$parkspace;
        }
        if ($request->input('fence')!='') {
            $fence=($request->input('fence')=='Yes')?'1':'0';
            $fen = array('fence','=',$fence);
            $tcoll[]=$fen;
        }
        $tcoll[]=array('notavailable','0');

        $spaces = Space::where($tcoll)->paginate(5);
        //return dd($spaces);
        $locations=AvailableLocation::orderBy('area')->get();
        $similar=0;
        if ($spaces->isEmpty()) {
            $spaces = Space::where($tcoll[0][0],$tcoll[0][2])->orderByRaw('RAND()')->paginate(5);
            $similar=1;
        }
        if ($spaces->isEmpty()) {
            $spaces = Space::orderByRaw('RAND()')->paginate(5);
            $similar=1;
        }
        $data=array('spaces' =>$spaces,'similar'=>$similar,'locations'=>$locations);
        if ($request->ajax()) {
            return view('searchresults.searchresult',compact(['spaces','similar']))->render();  
        }
        
        return view('search', compact(['spaces','similar','locations']));
        
    }

    public function location()
    {
        return view('search');
    }
    public function registerinterest(Request $request)
    {
        $spacereg=new SpaceRegister;
        $spacereg->phone=$request->input('phone');
        $spacereg->location=$request->input('location');
        $spacereg->duration=$request->input('duration');
        $spacereg->spacetype=$request->input('spacetype');
        $spacereg->price=$request->input('price');
        $spacereg->save();
        return redirect()->back()->with('status','Request received successfully, we would contact you soon once your space is found. Thank you');
    }
}
