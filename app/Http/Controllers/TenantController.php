<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenant;
use App\AvailableLocation;
use App\Space;
use App\Bookmark;
use App\SpaceBookingTransaction;
use Illuminate\Support\Facades\Auth;

class TenantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tenant');
    }

    public function setbookmark(Request $request)
    {
        //check bookmark to see if already exist
        $bkmark=Bookmark::where([
                    ['tenantid', '=', $request->input('tid')],
                    ['spaceid', '=', $request->input('sid')],
                ])->get();
        if ($bkmark->isEmpty()) {
            //create a new one
            $bk=new Bookmark;
            $bk->tenantid=$request->input('tid');
            $bk->spaceid=$request->input('sid');
            $bk->unbookmark=0;
            $bk->save();
            return "bookmarked";
        }else{
            //update the existing model
            $bk=Bookmark::find($bkmark[0]->id);
            $bk->unbookmark=0;
            $bk->save();
            return "bookmarked";
        }
        
    }

    public function unsetbookmark(Request $request)
    {
        //check bookmark to see if already exist
        $bkmark=Bookmark::where([
                    ['tenantid', '=', $request->input('tid')],
                    ['spaceid', '=', $request->input('sid')],
                ])->get();
        //update the existing model
        $bk=Bookmark::find($bkmark[0]->id);
            $bk->unbookmark=1;
            $bk->save();
            return "bookmark";
        
    }

     public function checkbookmarks()
    {
        //get auth id
        $tenantid=Auth::id();

        //Check for bookmarks
        $bookmarks=Bookmark::where([
                    ['tenantid', '=', $tenantid],
                    ['unbookmark', '=', 0],
                ])->paginate(5);
        $data = array('bookmarks' =>$bookmarks);
        $locations=AvailableLocation::orderBy('area')->get();
        return view('bookmark', compact(['bookmarks','locations']));
    }

     public function search(Request $request)
    {
        //get auth id
        $tenantid=Auth::id();
        $tcoll=array();
        //Prepare the search variables
        if ($request->input('location')!='') {
            $lc = array('area','=',$request->input('location'));
            $tcoll[]=$lc;
        }
        if ($request->input('spacetype')!='') {
            $spacetype=($request->input('spacetype')=='Single Space')?1:2;
            $stype = array('spacetypeid','=',$spacetype);
            $tcoll[]=$stype;
            }
        if ($request->input('duration')!='') {
            $frequency=($request->input('duration')=='Less than a month')?'night':'month';
            $pf = array('paymentfrequency','=',$frequency);
            $tcoll[]=$pf;
        }
        if ($request->input('minprice')!='') {
            $minprice=$request->input('minprice');
            $minp = array('price','>=',$minprice);
            $tcoll[]=$minp;
        }
        if ($request->input('maxprice')!='') {
            $maxprice=$request->input('maxprice');
            $maxp = array('price','<=',$maxprice);
            $tcoll[]=$maxp;
        }
        if ($request->input('guard')!='') {
            $guard=($request->input('guard')=='Yes')?'1':'0';
            $gd = array('guard','=',$guard);
            $tcoll[]=$gd;
        }
        if ($request->input('parkingspace')!='') {
            $parkingspace=($request->input('parkingspace')=='Yes')?'1':'0';
            $parkspace = array('parkingspace','=',$parkingspace);
            $tcoll[]=$parkspace;
        }
        if ($request->input('fence')!='') {
            $fence=($request->input('fence')=='Yes')?'1':'0';
            $fen = array('fence','=',$fence);
            $tcoll[]=$fen;
        }
        $tenid = array('id','=','0');
        $tcoll[]=$tenid;
        $matched=array();
        

        //Check for bookmarks
        $bookmarks=Bookmark::where([
                    ['tenantid', '=', $tenantid],
                    ['unbookmark', '=', 0],
                ])->get();
        if ($bookmarks->count()>0) {
            for ($i=0; $i <$bookmarks->count() ; $i++) { 
                $tcoll[count($tcoll)-1][2]=$bookmarks[$i]->spaceid;
                $bks=$bookmarks[$i]->space()->where($tcoll)->get();
                if ($bks->count()>0) {
                    $matched[]=$bookmarks[$i]->id;
                }
            }
            
                $bookmarks=Bookmark::whereIn('id',$matched)->paginate(10);
            
        } else {
            $bookmarks=Bookmark::where([
                    ['tenantid', '=', $tenantid],
                    ['unbookmark', '=', 0],
                ])->paginate(5);
        }
        
        $data = array('bookmarks' =>$bookmarks);
        $locations=AvailableLocation::orderBy('area')->get();
        return view('bookmark', compact(['bookmarks','locations']));
    }
    public function checkpaymenthistory()
    {
        //Get auth id of the tenant
        $tenantid=Auth::id();

        //Check for payment history and send to the view
        $payment_history=SpaceBookingTransaction::where('tenantid',$tenantid)->get();
        $data = array('history' => $payment_history);
        return view('payment',$data);
    }
}
