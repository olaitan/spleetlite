<?php

namespace App\Http\Controllers;

use App\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TenantRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/spaces';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

 

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
    	 $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:tenants',
            'password' => 'required|string|min:6',
        ]);
    	

    	if($validator->fails()){
    		 return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
    	}else{
    		 $tenant = new Tenant;

	        $tenant->email = $request->input('email');
	        $tenant->password=bcrypt($request->input('password'));
	        $tenant->save();
    		
    		return redirect($request->route);
    	}
        
    }
}
