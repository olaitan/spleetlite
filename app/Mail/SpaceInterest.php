<?php

namespace App\Mail;

use App\SpaceRegister;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SpacesInterest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $spaceregister;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SpaceRegister $sr)
    {
        $this->spaceregister = $sr;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@spleet.ng')
                    ->subject('Space Interest On Spleet.ng')
                    ->view('spaceinterestmail');
    }
}
