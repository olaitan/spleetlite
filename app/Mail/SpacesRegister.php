<?php

namespace App\Mail;

use App\PotentialHomeowner;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SpacesRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $potentialhomeowner;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PotentialHomeowner $ph)
    {
        $this->potentialhomeowner = $ph;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@spleet.ng')
                    ->subject('Space Registering Application')
                    ->view('mail');
    }
}
