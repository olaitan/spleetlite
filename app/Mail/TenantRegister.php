<?php

namespace App\Mail;

use App\Tenant;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TenantRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $tenant;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Tenant $t)
    {
        $this->tenant = $t;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@spleet.ng')
                    ->subject('Welcome To Spleet.ng')
                    ->view('signupmail');
    }
}
