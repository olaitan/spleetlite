<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PotentialHomeowner extends Model
{
    public $timestamps=false;
    protected $table='potentialhomeowners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'email', 'mobile','address','time','date','photodirectory',
    ];



}
