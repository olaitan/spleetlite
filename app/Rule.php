<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    /**
    *The model associated with the Rules table
    *
    */
    
    public $timestamps=false;

    //One to many(inverse) relationship with space
    public function space()
    {
    	return $this->belongsTo('App\Space','spaceid');
    }
}
