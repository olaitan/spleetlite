<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    /**
    *The model associated with the Spaces table
    *
    */
    
    public $timestamps=false;

    //Inverse Relationships(One to many)
    //One to many(inverse) relationship with homeowner
    public function homeowner()
    {
    	return $this->belongsTo('App\HomeOwner','homeownerid');
    }

    //One to many(inverse) relationship with spacetype
    public function spacetype()
    {
        return $this->belongsTo('App\SpaceType','spacetypeid');
    }

    //One to many(inverse) relationship with location
    public function location()
    {
    	return $this->belongsTo('App\Location','locationid');
    }


    //One to many Relationships
    //One to many relationship with spacegallery
    public function spacegalleries()
    {
    	return $this->hasMany('App\SpaceGallery','spaceid');
    }
    //One to many relationship with rules
    public function rules()
    {
    	return $this->hasMany('App\Rule','spaceid');
    }
    //One to many relationship with spacecontract
    public function spacecontracts()
    {
    	return $this->hasMany('App\SpaceContract','spaceid');
    }
    //One to many relationship with bookmark
    public function bookmarks()
    {
        return $this->hasMany('App\Bookmark','spaceid');
    }
    //One to many relationship with spacebookingtransaction
    public function spacebookingtransactions()
    {
    	return $this->hasMany('App\SpaceBookingTransaction','spaceid');
    }

    //Many to many relationship with amenity
    public function amenities()
    {
    	return $this->belongsToMany('App\Amenity','spaceamenities','spaceid','amenityid')->withPivot('number');
    }

}
