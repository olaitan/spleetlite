<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceBookingTransaction extends Model
{
    /**
    *The model associated with the SpaceBookingTransactions table
    *
    */
    protected $table='spacebookingtransactions';
    public $timestamps=false;

    //One to many(inverse) relationship with space
    public function space()
    {
    	return $this->belongsTo('App\Space','spaceid');
    }
    //One to many(inverse) relationship with tenant
    public function tenant()
    {
    	return $this->belongsTo('App\Tenant','tenantid');
    }
}
