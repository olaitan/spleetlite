<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceGallery extends Model
{
    /**
    *The model associated with the SpaceGalleries table
    *
    */
    protected $table='spacegalleries';
    public $timestamps=false;

    //One to many(inverse) relationship with space
    public function space()
    {
    	return $this->belongsTo('App\Space','spaceid');
    }
}
