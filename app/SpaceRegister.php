<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceRegister extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'phone', 'location','duration','spacetype','price',
    ];

    protected $table='spaceinterests';

}
