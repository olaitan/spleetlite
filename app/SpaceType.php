<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceType extends Model
{
    /**
    *The model associated with the Amenities table
    *
    */
    protected $table='spacetype';
    public $timestamps=false;

    //One to many Relationships
    //One to many relationship with spaces
    public function spaces()
    {
    	return $this->hasMany('App\Space','spacetypeid');
    }
}
