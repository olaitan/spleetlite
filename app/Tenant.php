<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Tenant extends Authenticatable
{
    /**
    *The model associated with the tenants table
    *
    */

    protected $guard = 'tenant';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'email', 'password', 'firstname','access_token','name','provider','provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps=false;

    //One to many relationship with spacecontract
    public function spacecontracts()
    {
    	return $this->hasMany('App\SpaceContract','tenantid');
    }

    //One to many relationship with bookmark
    public function bookmarks()
    {
        return $this->hasMany('App\Bookmark','tenantid');
    }

    //One to many relationship with spacebookingtransaction
    public function spacebookingtransactions()
    {
    	return $this->hasMany('App\SpaceBookingTransaction','tenantid');
    }
}
