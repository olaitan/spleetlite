$(window).ready(function(){   
    $(window).scroll(function(){
        if ($(document).scrollTop()>80){
            $("#navigation").addClass("active animated fadeInDown");  
        } else{
            $("#navigation").removeClass("active animated fadeInDown");
        }
    })
    $(".jumbotron").addClass("animated bounceInLeft");
    $("#sidebar").addClass("animated bounceInDown");
    $(".search-row").addClass("animated bounceInDown");
    

    /* 
        CONTROL FOR THE MENU DROP DOWN
    */
    $(".mySpleet").bind('click', function(){
        $(".menulist").toggleClass("active");
        $(".menulist li a").bind('click', function(){            
            $(".menulist").toggleClass("active");
        })
    })
    /* 
        THIS SECTION HANDLES THE CHANGE OF PICTURE IN SEARCH VIEW
    */
    var searchImages = $(".space-images");
    var n = 0;
    var q = 0;
    var newLinker = '';
    $(searchImages).each(function(e){
        n++;
        $(this).attr("id", "imageRow"+n);
        var newLinker = ("#"+$(this).attr("id"));
        var images =[]= $(newLinker + " img");
        var img = $(newLinker + " img"+[]);
        function imageChanger(){
            $(newLinker + " img").removeClass("active");
            $(img[q]).addClass("active animated slideInLeft");  
        }
        $(newLinker+ " .rightbtn").bind('click', function(){
            q++;
            if (q >images.length-1){
                q = images.length-1; 
            }     
            imageChanger(q);
        })
        $(newLinker+ " .leftbtn").bind('click', function(){
            if (q == 0){
                q = images.length;
            } 
            q--;
            imageChanger(q);
        }) 
    })

    
    /* 
        CONTROLLING THE POP UP MESSAGES
    */
    
    $("div#pop-up div.closeIcon").bind('click', function(){
        $("div#pop-up").removeClass("active");
    })

    /* 
        THIS SECTION HANDLES CHANGE OF IMAGES IN SPACE OVER VIEW
    */
    var spaceLeftBtn = $("#imageWrapper .leftButton");
    var spaceRightBtn = $("#imageWrapper .rightButton");
    var spaceImages = [] = ($("#imageWrapper img"));
    var i = 0;
    function imageChange(i){
        $("#imageWrapper img").removeClass("active");
        $(spaceImages[i]).addClass("active animated fadeIn");
    }
    spaceRightBtn.bind('click', function(){
        i++;
        if (i > spaceImages.length-1){
            i = spaceImages.length-1;
        }
        imageChange(i);
    })
    spaceLeftBtn.bind('click', function(){
        if(i == 0){
            i = spaceImages.length;
        }
        i--;
        imageChange(i);
    })
    
    /* 
        THIS IS THE REVISED HANDLER FOR THE TAB VIEW IN SPACE
    */
    var eachTab = $(".tabs li h4");
    eachTab.bind('click', function(){
        eachTab.removeClass("active");
        $(this).toggleClass("active");
        var idString = $(this, "h4[id]");
        var newId = idString.attr("id");
        $(".tab-contents div").removeClass("active");
        $("div#"+newId).addClass("active animated fadeInRight");
    })

    
    /* 
        THIS SECTION HANDLES THE EXPANDED AND MINIFIED FORM ON THE SEARCH PAGE
    */
    var currentSelect = $("#formRow form div div span");
    var searchList = $("#formRow form div div ul.searchList");    
    currentSelect.bind('click', function(){
        var currentID = ("#"+$(this).attr("id"));
        $("ul.searchList").removeClass("active");
        $(currentID+ " ul.searchList").addClass("active");        
        var newValue = $(currentID+" ul.searchList li p");
        $(newValue).bind('click', function(){
            var newText = $(this).text();
            $("span"+currentID).text(newText);            
            $("input"+currentID).val(newText);
        }) 
        $(function(){        
            $(searchList).click(function(){
                $(searchList).removeClass("active");
            })
        }) 
    })   

    $("#sort-by span").bind('click', function(){
        $("#sort-by .sortList").toggleClass("active");
        $(".sortList li p").bind('click', function(){
            $("#sort-by span").text($(this).text());
        })
        $(function(){        
            $(".sortList").click(function(){
                $(".sortList").removeClass("active");
            })
        }) 
    })


    /* 
        THIS SECTION HANDLES THE PAGINATION and NAVIGATION LINKS
    */
    var searchLink = $(".pagination ul li a");
    var navLinks= $(".navigationLinks ul li a"); 

    searchLink.bind('click', function(){
        searchLink.removeClass("active");
        $(this).addClass("active");
    })
    navLinks.bind('click', function(){
        navLinks.removeClass("active");
        $(this).addClass("active");
    })

    $(".menu-bar").bind('click', function(){
        $(this).toggleClass("change");
        $("#nav").toggleClass("active");
    })
    
    $("#contacts").bind("click", function(){
        $(".list").toggleClass("active");
     })

    /* Register a space function */
    $(".registerSpace").bind('click', function(){
        $("body").css({"overflow-y":"hidden"});
        $("<style>body::after{display: block !important}</style>").appendTo("head");
        document.getElementById("formSpaceRegister").style.display = "block";
    })
    /* sign out function */

    /* sign in function */
    $(".signin").bind('click', function(){
        $("body").css({"overflow":"hidden"});
        $("<style>body::after{display: block !important}</style>").appendTo("head");
        document.getElementById("signin-option").style.display = "block";
    })    

    $(".signin").bind('click', function(){
		$("body").css({"overflow":"hidden"});
		$('<style>body::after{display: block}</style>').appendTo('head');		
		$('.close').parent().css({"display": 'none'});	
		document.getElementById("signin-option").style.display = "block";
    })

    /* sign up function */
    $(".signupBtn").bind('click', function(){
        $("body").css({"overflow":"hidden"});
        $("<style>body::after{display: block !important}</style>").appendTo("head");
        document.getElementById("signup-options").style.display = "block";
    })

    /* Email sign up option */
    $(".emailOpt").bind('click', function(){		
		$("body").css({"overflow":"hidden"});
		$('<style>body::after{display: block}</style>').appendTo('head');	
		$('.close').parent().css({"display": 'none'});	
		document.getElementById("register-option").style.display = "block";
    })

    /* Sign up optins function */
    $("#signupOpts").bind('click', function(){
		$("body").css({"overflow":"hidden"});
		$('<style>body::after{display: block}</style>').appendTo('head');		
		$('.close').parent().css({"display": 'none'});	
		document.getElementById("signup-options").style.display = "block";
    })

    /* closing of each tabs */
    $(".close").bind('click', function(){
		$("body").css({"overflow-y" : "scroll"});
		$('.close').parent().css({"display": 'none'});		
		$('<style>body::after{display: none !important}</style>').appendTo('head');	
    })
    
    $(".addMoreSpace").bind('click', function(){
        if($(".space").length < 5){
            $(".spaceDetails").append($('<div class="space animated fadeIn"><div><i class="fa fa-street-view fa-2x"></i></div><input type="text" name="spaceAddress" id="" placeholder="enter space address"><div class="dateAndTime"><div><span>Time</span><input type="time" name="" id="" placeholder="00 : 00 : 00" /></div><div><span>Date</span><input type="date" name="" id="" placeholder="DD / MM / YYYY "></div></div></div>'));
        } else {
            alert("Sorry, you can't add more spaces now.");
        }
    })

    /* BOOKMARK EFFECTS */    
    var currentSelectBS = $(".bookmarkSearch form.BS div div span");
    var searchListBS = $(".bookmarkSearch form.BS div div ul.searchList");    
    //var activeSearchList = $(currentID+ " ul.searchList").hasClass("active");  
    currentSelectBS.bind('click', function(){
        var currentID = ("#"+$(this).attr("id"));
        if ($("ul.searchList").hasClass("active")){
            $("ul.searchList").removeClass("active");
        } else{
            var activeSearchList = $(currentID+ " ul.searchList").addClass("active");   
        }          
        var newValueBS = $(currentID+" ul.searchList li p");
        $(newValueBS).bind('click', function(){
            $(activeSearchList).toggleClass("active");
            var newTextBS = $(this).text();
            $("span"+currentID).text(newTextBS);
            $("input"+currentID).val(newTextBS);
        }) 
        $(searchListBS).click(function(){
            $(searchListBS).removeClass("active");
        })
        $(function(){        
            $(window).click(function(){
                if ($(activeSearchList).hasClass("active")){
                    $(activeSearchList).removeClass("active");
                }
            })
        })  
    }) 


    /* SEARCH ROW EFFECTS */    
    var currentSelectJS = $(".search-row form.JS div span");
    var rowListJS = $(".search-row form.JS div ul.rowList"); 
    currentSelectJS.bind('click', function(){
        
        var currentIDD = ("#"+$(this).attr("id"));
        if ($("ul.rowList").hasClass("active")){
            $("ul.rowList").removeClass("active");
        } 
        $(currentIDD+ " ul.rowList").addClass("active");
        var newValueJS = $(currentIDD+" ul.rowList li p");
        $(newValueJS).bind('click', function(){
            var newTextJS = $(this).text();
            $("span"+currentIDD).text(newTextJS);
            $("input"+currentIDD).val(newTextJS);
        })  

        /* $(currentSelectJS).bind('click', function(){
            $(this).bind('click', function(){
                $(currentIDD+ " ul.rowList").toggleClass("active");
            })
        }) */
        $(window).click(function(){                 
            $(currentIDD+ " ul.rowList").click(function(){
                $(this).removeClass("active");                
            })
        }) 
    })
    
})