##Wikipedia
------------------------------------------------------------------------------------------------------------------------------
##About Spleet
Spleet is a spaces/estate management company. SpleetLite is a lite version of spleet.ng. Built with laravel, vue.js, html and css.


##Recent Updates In The Configuration
** You need to update the database with the new cloned spleetdb and drop the formal, this is due to the social authentication added*****


      ##Contents
1. Tools needed to run the app
2. Tools Setup
3. Project Setup
4. Run Project
5. HomePage
6. Spaces
7. Profile



##Tools Needed To Run The App
You will need php7+ version installed on your computer.
You will need MySql installed on your computer
Download and install Xamp(It contains all that you need including php7+ and MySql)

##Tools Setup
Install Xamp and launch it.
The Xampp Control panel is opened, Start the Apache module and the MySql module.
Click the admin button on the MySql module to see the PhpAdmin that manages MySql, here is where the database would be managed.

##Project Setup
After cloning/download the project, open the spleetdb folder. You would find the sql export db there.
Go to PhpAdmin for MySql and import the file to create the database in your local server.

##Run Project
Start the Apache module and MySql module if not started.
Open the path to the root folder of the project in command prompt and enter "php artisan serve" to run the App.
It returns the a link, Copy and paste on your browser.





