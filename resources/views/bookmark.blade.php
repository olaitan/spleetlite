<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bookmark.css')}}">
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <title>Spleet.ng - Life Together</title>
</head>
<body>
    <!-- NAVIGATION RULE -->
    <div id="navigation">
        <div class="companyLogo">
            <a href="{{route('index')}}">
                <img src="{{asset('assets/icons/spleet-2-2@2x.png')}}" alt="Spleet-Ng" />
            </a>
        </div>
        <div class="navigationLinks" id="nav">
            <ul>                
                @auth
                <li><a class="mySpleet">My Spleet</a>
                    <ul class="menulist">                            
                        <li><a href="{{route('user.bookmarks')}}">Bookmarks</a></li>
                        <li><a href="{{route('payment.history')}}">Payment History</a></li>
                        <li><a href="{{route('tenant.logout')}}" class="signout">Logout</a></li>
                    </ul>
                </li>                     
                @else         
                    <li><a href="#" class="signin">Sign In</a></li>
                    <li class="noHover">
                        <div class="signupBtn">
                            <a href="#">Sign Up</a>
                        </div>
                    </li>
                @endauth
            </ul>
        </div>
        <!-- hamburger -->
        <div class="menu-bar">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
    </div>

    <div class="bookmarkSearch">
        <form action="{{route('user.search')}}" method="get" name="bs" class="BS">   
            {{csrf_field()}} 
                <div class="location">
                    <p>location</p>
                    <div id="location">
                        <span id="location">location</span>
                        <input id="location" name="location" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            @foreach ($locations as $location)
                            <li><p>{{ $location->area }}</p></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
                <div class="duration">
                    <p>duration</p>
                    <div id="duration">
                        <span id="duration">Duration</span>
                        <input id="duration" name="duration" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Less than a month</p></li>
                            <li><p>More than a month</p></li>
                        </ul>
                    </div>
                </div>
                <div class="min-price">
                    <p>min. price</p>
                    <div id="min-price">
                        <input id="min-price" name="minprice" type="number" value="" placeholder="60000"/>
                    </div>
                </div>
                <div class="max-price"> 
                    <p>max. price</p>
                    <div id="max-price">
                        <input id="max-price" name="maxprice" type="number" value="" placeholder="100000"/>
                    </div>   
                </div>
                <div class="guard">
                    <p>guard</p>
                    <div id="guard">
                        <span id="guard">Select</span>
                        <input id="guard" name="guard" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Yes</p></li>
                            <li><p>No</p></li>
                        </ul>
                    </div>
                </div>
                <div class="type">
                    <p>type</p>
                    <div id="type">
                        <span id="type">Select</span>
                        <input id="type" name="spacetype" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Single Space</p></li>
                            <li><p>Multi Space</p></li>
                        </ul>
                    </div>
                </div>
                <div class="parking-space">
                    <p>parking space</p>
                    <div id="parking-space">
                        <span id="parking-space">Select</span>
                        <input id="parking-space" name="parkingspace" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Yes</p></li>
                            <li><p>No</p></li>
                        </ul>
                    </div>
                </div>
                <div class="fence">
                    <p>fenced</p>
                    <div id="fence">
                        <span id="fence">Select</span>
                        <input id="fence" name="fence" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Yes</p></li>
                            <li><p>No</p></li>
                        </ul>
                    </div>             
                </div>
                <div class="button">
                    <input type="submit" value="search" />
                </div>
        </form>
    </div>

    @if (count($bookmarks) > 0)
                    <section class="myspaces">
                        @include('searchresults.bookmarkresult')
                    </section>
      @endif
        
    <script src="{{asset('js/main.js')}}"></script>
</body>
</html>