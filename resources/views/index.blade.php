<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/font-awesome.min.css" >
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/mainindex.css">
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <title>Spleet.ng - Life Together</title>
</head>
<body>
    <!-- REGISTER SPLEET -->
    <div id="formSpaceRegister">
        <div class="close"></div>
        <div class="header"><h4>Register a Space</h4></div>
        <span>Please fill in the following details correctly</span>
        <form class="spaceRegister" action="{{route('spaceregister')}}" method="post" name="spaceRegister">
            {{csrf_field()}}
        <div class="name">
            <div><i class="fa fa-user-o fa-2x"></i></div>
            <input type="text" name="fullname" placeholder="full name" required>
        </div>
        <div>
            <div><i class="fa fa-envelope-o fa-2x"></i></div>
            <input type="email" name="email" placeholder="Email address" required >
        </div>
        <div class="phoneNumber">
            <div><i class="fa fa-phone fa-2x"></i></div>
            <input type="tel" name="phone" placeholder="0800000XXXX" required>
        </div>
        <div class="spaceDetails">
            <div class="space">
                <div><i class="fa fa-street-view fa-2x"></i></div>
                <input type="text" name="spaceAddress" placeholder="enter space address" required>
                <div class="dateAndTime">
                    <div>
                        <span>Time</span>
                        <input type="time" name="time" placeholder="00 : 00 : 00" required />
                    </div>
                    <div>
                        <span>Date</span>
                        <input type="date" name="date" placeholder="DD / MM / YYYY " required>
                    </div>
                </div>
            </div>
        </div>
        <div class="addMoreSpace">
            <div><i class="fa fa-plus-square-o fa-2x"></i></div>
            <h5>Add more space</h5>
        </div>
        <div class="spaceSubmit">
            <input type="submit" value="Submit my Space">
        </div>
        </form>
    </div>
    <div class="formRow" id="formRow">
        <!-- SIGN UP OPTIONS -->
        <div id="signup-options">
            <div class="close"></div>
            <p>sign up</p>
            <div class="facebook">
                <i class="fa fa-facebook-official fa-2x"></i>
                <p><a href="oauth/facebook">Sign up with Facebook</a></p>
            </div>
            <div class="google"><i class="fa fa-google fa-2x"></i><p><a href="oauth/google">Sign up with Google</a></p></div>
            <span>or</span>
            <div class="emailOpt"><i class="fa fa-envelope-o fa-2x"></i><p>Sign up with Email</p></div>
            <p class="agree">By signing up, you accept our Terms of Use and Privacy Policy.</p>
            <div class="baseOpt">
                <p>Already have a Spleet account?</p>
                <input type="submit" class="signin" value="sign in" />
            </div>
        </div>

        <!-- SIGNING IN OPTIONS -->
        <div id="signin-option">
            <div class="close"></div>
            <p>sign in</p>
            <div class="socialmedia-option">
                <div class="facebookOpt">
                    <i class="fa fa-facebook-official fa-2x"></i>
                    <p><a href="oauth/facebook">with Facebook</a></p>
                </div>
                <div class="googleOpt">
                    <i class="fa fa-google fa-2x"></i>
                    <p><a href="oauth/google">with Google</a></p>
                </div>
            </div>
            <span>or with email</span>
            <form method="post" action="{{route('tenant.login.submit')}}" class="sigin">
                {{ csrf_field() }}
                <input type="text" hidden="hidden" name="route" value="/spaces">
                <div class="email">
                    <input type="email" name="email" placeholder="Enter your Email Address" required />
                </div>
                <div class="password">
                    <input type="password" name="password" placeholder="Password" required />
                </div>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="rememberme" /><p>Keep me signed in</p>
                <div class="submitBtn">
                    <input type="submit" class="submit" value="sign in" />
                </div>
            </form>
            <p>I forgot my password</p>
            <div class="base">
                <p>Don't have a Spleet account?</p>
                <input type="submit" id="signupOpts" value="sign up" />
            </div>
        </div>

        <!-- JOINING SPLEET -->
        <div id="register-option">
            <div class="close"></div>
            <p>join spleet</p>
            <form method="post" action="{{route('tenant.signup.submit')}}">
                {{ csrf_field() }}
                <input type="" hidden="hidden" name="route" value="/spaces">
                <div>
                    <input type="email" name="email" value="" placeholder="Email Address" required>
                </div>
                <div>
                    <input type="password" name="password" value="" placeholder="Pasword" required>
                </div>
                <div>
                    <input type="submit" name="submit" value="sign up">
                </div>
            </form>
            <p>By signing up, you accept our <span>Terms of Use</span> and <span>Privacy Policy.</span></p>
            <div class="baseOpt">
                <p>Already have a Spleet account?</p>
                <input type="submit" class="signin submit" value="sign in" />
            </div>
        </div>
    </div>

    @if ($errors->any())
        <!-- POP-UP FOR ERRORS -->
    <div id="pop-up" style="background: red" class="active">
        <div class="errorMsg"><p style="color: white">Incorrect Username/Password Combination</p></div>
        <div class="closeIcon"><i class="fa fa-close fa-2x"></i></div>
    </div>
    @endif
    @if (session('status'))
    <!-- POP-UP FOR SPACEREGISTER -->
    <div id="pop-up"  class="active">
        <div class="errorMsg"><p style="color: white">The Space/Spaces upload request have been successfully submitted. We will get in touch soon.</p></div>
        <div class="closeIcon"><i class="fa fa-close fa-2x"></i></div>
    </div>
    @endif

    <!-- MAIN WRAP -->
    <div id="mainWrap">
        <!-- NAVIGATION RULE -->
        <div id="navigation">
            <div class="companyLogo">

                <a href="{{route('index')}}">
                    <img src="assets/icons/spleet-2-2@2x.png" alt="Spleet-Ng" />
                </a>
            </div>
            <div class="navigationLinks" id="nav">
                <ul>                
                    @auth
                    <li><a class="mySpleet">My Spleet</a>
                        <ul class="menulist">                            
                            <li><a href="{{route('user.bookmarks')}}">Bookmarks</a></li>
                            <li><a href="{{route('payment.history')}}">Payment History</a></li>
                            <li><a href="{{route('tenant.logout')}}" class="signout">Logout</a></li>
                        </ul>
                    </li>             
                    <li><a href="#" class="registerSpace">Register a Space</a></li>        
                    @else                        
                        <li><a href="#" class="registerSpace">Register a Space</a></li> 
                        <li><a href="#" class="signin">Sign In</a></li>
                        <li class="noHover">
                            <div class="signupBtn">
                                <a href="#">Sign Up</a>
                            </div>
                        </li>
                    @endauth
                </ul>
            </div>
            <!-- hamburger -->
            <div class="menu-bar">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>
        <!-- JUMBOTRON -->
        <div class="jumbotron">
            <p>Life, Together</p>
            <p>Staying for a <span>few nights</span> or <br>
                <span>longer term with a pay monthly,</span><br>
                <span style="font-weight: 700;">Spleet</span> has you covered.
            </p>
        </div>
        <!-- SEARCH ROW -->
        <div class="search-row">
            <form method="get" action="{{route('search')}}" name="space" class="JS">
                {{csrf_field()}}
                <div id="location" class="row">
                    <div><i class="fa fa-map-marker fa-2x"></i></div>
                    <span id="location">location</span>
                    <input id="location" name="location" hidden="hidden" type="text" value="" />
                    <ul class="rowList">
                        @foreach ($locations as $location)
                            <li><p>{{ $location->area }}</p></li>
                        @endforeach
                    </ul>
                </div>
                <div id="spaces" class="row">
                    <div><i class="fa fa-hotel fa-2x"></i></div>
                    <span id="spaces">space type</span>
                    <input id="spaces" name="spacetype" hidden="hidden" type="text" value="" />
                    <ul class="rowList">
                        <li><p>Single Space</p></li>
                        <li><p>Multi Space</p></li>
                    </ul>
                </div>
                <div id="duration" class="row">
                    <div><i class="fa fa-clock-o fa-2x"></i></div>
                    <span id="duration">duration</span>
                    <input id="duration" name="duration" hidden="hidden" type="text" value="" />
                    <ul class="rowList">
                        <li><p>Less than a month</p></li>
                        <li><p>More than a month</p></li>
                    </ul>
                </div>
                <div class="formIn">
                    <input type="submit" value="search" />
                </div>
            </form>
        </div>
    </div>

    <!-- FEATURE ROW -->
    <div id="features">
        <div class="features-header">
            <!-- <p>features</p> -->
        </div>
        <div class="feature-column" id="duration">
            <div class="caption">duration</div>
            <div class="body"><p>On <span>Spleet</span>, you can find spaces that satisfy your needs for as short as a few nights or as long as a year</p></div>
            <div class="icons"><i class="fa fa-clock-o fa-3x" aria-hidden="true"></i></div>
        </div>
        <div class="feature-column" id="payment">
            <div class="caption">payment</div>
            <div class="body"><p>On <span>Spleet</span>, you can occupy spaces for as long as a year & pay monthly</p></div>
            <div class="icons"><i class="fa fa-credit-card fa-3x" aria-hidden="true"></i></div>
        </div>
        <div class="feature-column" id="quality">
            <div class="caption">quality</div>
            <div class="body"><p>On <span>Spleet</span>, we offer world class services and you can be assured we will always serve you to the best of our abilities</p></div>
            <div class="icons"><i class="fa fa-star-o fa-3x" aria-hidden="true"></i></div>
        </div>
    </div>

    <!-- TRENDING ROW -->
    <div id="trending">
        <div class="trending-header">
            <p>trending <span>spaces</span></p>
        </div>
        <div class="trending-column">
            <div class="icons"><i class="fa fa-home fa-2x"></i></div>
            <div class="space">
                <p>{{$spaces[0]->name}}</p>
                <p><span>{{$spaces[0]->location->area}}<span> <strong>.</strong> NGN {{number_format($spaces[0]->price,0,'.',',')}} per {{$spaces[0]->paymentfrequency}}</p>
            </div>
            <div class="spaceDetails">
                <input type="submit" value="Space details" onclick="location.href = '{{route('space',$spaces[0]->id)}}';"/>
                <i class="fa fa-ellipsis-h fa-2x" aria-hidden="true" onclick="location.href = '{{route('space',$spaces[0]->id)}}';"></i>
            </div>
        </div>

        <div class="trending-column">
            <div class="icons"><i class="fa fa-home fa-2x"></i></div>
            <div class="space">
                <p>{{$spaces[1]->name}}</p>
                <p><span>{{$spaces[1]->location->area}}<span> <strong>.</strong> NGN {{number_format($spaces[1]->price,0,'.',',')}} per {{$spaces[1]->paymentfrequency}}</p>
            </div>
            <div class="spaceDetails">
                <input type="submit" value="Space details" onclick="location.href = '{{route('space',$spaces[1]->id)}}';"/>
                <i class="fa fa-ellipsis-h fa-2x" aria-hidden="true" onclick="location.href = '{{route('space',$spaces[1]->id)}}';"></i>
            </div>
        </div>
        <div class="trending-column">
            <div class="icons"><i class="fa fa-home fa-2x"></i></div>
            <div class="space">
                <p>{{$spaces[2]->name}}</p>
                <p><span>{{$spaces[2]->location->area}}<span> <strong>.</strong> NGN {{number_format($spaces[2]->price,0,'.',',')}} per {{$spaces[2]->paymentfrequency}}</p>
            </div>
            <div class="spaceDetails">
                <input type="submit" value="Space details" onclick="location.href = '{{route('space',$spaces[2]->id)}}';"/>
                <i class="fa fa-ellipsis-h fa-2x" aria-hidden="true" onclick="location.href = '{{route('space',$spaces[2]->id)}}';"></i>
            </div>
        </div>
        <div class="trending-column">
            <div class="icons"><i class="fa fa-home fa-2x"></i></div>
            <div class="space">
                <p>{{$spaces[3]->name}}</p>
                <p><span>{{$spaces[3]->location->area}}<span> <strong>.</strong> NGN {{number_format($spaces[3]->price,0,'.',',')}} per {{$spaces[3]->paymentfrequency}}</p>
            </div>
            <div class="spaceDetails">
                <input type="submit" value="Space details" onclick="location.href = '{{route('space',$spaces[3]->id)}}';"/>
                <i class="fa fa-ellipsis-h fa-2x" aria-hidden="true" onclick="location.href = '{{route('space',$spaces[3]->id)}}';"></i>
            </div>
        </div>
    </div>

    <!-- REVIEW ROW -->
    <div id="reviews">
        <div class="reviews-header">
            <p>What are they saying <span>about us?</span></p>
        </div>
        <div class="r">            
            <div class="review">
                <div class="row">
                    <div class="reviewerImage">
                        <img src="{{asset('assets/images/2.png')}}" alt="review" />
                    </div>
                </div>
                <div class="reviewContent">
                    <div class="quote"><i class="fa fa-quote-right fa-2x" aria-hidden="true"></i></div>
                    <div class="content">
                        <p class="content">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea illum ad obcaecati molestias provident accusamus quis, ratione unde molestiae, delectus impedit distinctio ipsam dolore itaque tempore sed numquam? Error, nisi.</p>
                        <p class="author">Adekunle Gold</p>
                    </div>
                </div>
            </div>
            <div class="review">
                <div class="row">
                    <div class="reviewerImage">
                        <img src="{{asset('assets/images/1.png')}}" alt="review" /></div>
                </div>
                <div class="reviewContent">
                    <div class="quote"><i class="fa fa-quote-right fa-2x" aria-hidden="true"></i></div>
                    <div class="content">
                        <p class="content">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea illum ad obcaecati molestias provident accusamus quis, ratione unde molestiae, delectus impedit distinctio ipsam dolore itaque tempore sed numquam? Error, nisi.</p>
                        <p class="author">Adekunle Gold</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- QUESTION rOW -->
    <div id="questions">
        <p>your questions about <span>spleet</span></p>
        <div class="question-answer">
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla id explicabo assumenda enim consequuntur ratione tempore fugiat aut voluptatem, ab eveniet eius nesciunt, reiciendis harum doloremque. Accusamus adipisci ad rerum?
            </p>
        </div>
    </div>

    <!-- PICTURE ROW -->
    <div id="picture">
        <div class="image" style="background-image: url('./assets/images/1.png')"></div>
        <div class="image" style="background-image: url('./assets/images/2.png')"></div>
        <div class="image" style="background-image: url('./assets/images/3.jpg')"></div>
    </div>

    <!-- FOOTER ROW -->
    <div id="footer">
        <div class="logo">
            <div class="spleetlogo"></div>
        </div>
        <div class="footer-content">
            <div class="footer-column">
                <h4>Spleet</h4>
                <ul>
                    <li><a href="http://">about</a></li>
                    <li><a href="http://">careers</a></li>
                    <li><a href="http://">blog</a></li>
                </ul>
            </div>
            <div class="footer-column">
                <h4>home owners</h4>
                <ul>
                    <li><a href="http://">welcome</a></li>
                    <li><a href="http://">sign up</a></li>
                </ul>
            </div>
            <div class="footer-column">
                <h4>company</h4>
                <ul>
                    <li><a href="http://">about</a></li>
                    <li><a href="http://">careers</a></li>
                    <li><a href="http://">blog</a></li>
                </ul>
            </div>
            <div class="footer-column">
                <h4>support</h4>
                <ul>
                    <li><a href="http://">support</a></li>
                    <li><a href="#questions">faq</a></li>
                </ul>
            </div>
            <div class="footer-column mobile">
                <div class="facebook"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></div>
                <div class="twitter"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></div>
                <div class="instagram"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></div>
                <div class="google"><i class="fa fa-google fa-2x" aria-hidden="true"></i></div>
            </div>
            <p>&copy; 2018 All Right Reserved.</p>
        </div>
    </div>

    <script src="js/main.js"></script>
    <script>
            var propImage = [];
            propImage = document.getElementsByClassName("image trendimage");

            propImage[0].style.backgroundImage = "url('storage/{{$spaces[0]->spacegalleries[0]->spacepic}}')";
            propImage[1].style.backgroundImage = "url('storage/{{$spaces[1]->spacegalleries[0]->spacepic}}')";
            propImage[2].style.backgroundImage = "url('storage/{{$spaces[2]->spacegalleries[0]->spacepic}}')";
            propImage[3].style.backgroundImage = "url('storage/{{$spaces[3]->spacegalleries[0]->spacepic}}')";
</script>
</body>
</html>
