<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/payment.css')}}">

    <script src="{{asset('js/jquery.js')}}"></script><!-- 
    <script src="js/jquery-3.2.1.min.js"></script> -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <title>Spleet.ng - Life Together</title>
</head>
<body>
    <!-- NAVIGATION -->
    <div id="navigation">
        <div class="companyLogo">
            <a href="{{route('index')}}">
                <img src="{{asset('assets/icons/spleet-2-2@2x.png')}}" alt="Spleet-Ng" />
            </a>
        </div>
        <div class="navigationLinks" id="nav">
            <ul>                
                @auth
                <li><a class="mySpleet">My Spleet</a>
                    <ul class="menulist">                            
                        <li><a href="{{route('user.bookmarks')}}">Bookmarks</a></li>
                        <li><a href="{{route('payment.history')}}">Payment History</a></li>
                        <li><a href="{{route('tenant.logout')}}" class="signout">Logout</a></li>
                    </ul>
                </li>                 
                @else                        
                    <li><a href="#" class="signin">Sign In</a></li>
                    <li class="noHover">
                        <div class="signupBtn">
                            <a href="#">Sign Up</a>
                        </div>
                    </li>
                @endauth
            </ul>
        </div>
        <!-- hamburger -->
        <div class="menu-bar">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
    </div>

    @if ($history->count()==0)
        <h1 style="text-align: center;margin-top: 50px">No Payment History</h1>
    @else
        <div class="payment">
        <div id="accordion" class="panel-group">

            @for ($x = 0; $x < $history->count(); $x++)
            <div class="panel custom-panel">   
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collapse{{$x}}" data-toggle="collapse" data-parent="accordion">Ref : {{$history[$x]->paymentreference}} --- {{$history[$x]->space->name}}</a> 
                    </h4>
                </div>
                <div id="collapse{{$x}}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="labels">
                            <p>location</p>
                            <p>space bought</p>
                            <p>amount paid</p>
                            <p>date/time</p>
                            
                        </div>
                        <div class="labelProperties">
                            <h4>{{$history[$x]->space->location->address}}</h4>
                            <h4>{{$history[$x]->space->name}}</h4>
                            <h4>NGN {{$history[$x]->amount}}</h4>
                            <h4>{{$history[$x]->bookingdate}}</h4>
                            
                        </div>
                    </div>
                </div>
            </div>
            @endfor

            
        
        </div>
    </div>
    @endif
    
    <script src="{{asset('js/payment.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>