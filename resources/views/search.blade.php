<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/searchstyles.css')}}">
    <style type="text/css">
        input[type=text],input[type=tel]{
            width: 48%;
            padding: 8px 16px;
            margin: 5px 4px;
            box-sizing: border-box;
            border: 1px solid #4f87fb;
            border-radius: 20px;
            background-color:#f4f1f3;
        }
        input[type=text]:focus,input[type=tel]:focus{
            width: 48%;
            padding: 7px 15px;
            margin: 5px 4px;
            box-sizing: border-box;
            border: 3px solid #ffffff;
            border-radius: 20px;
            background-color:#fafafa;
        }
        select{
            width: 48%;
            margin: 6px 4px;
            padding: 8px 16px;
            box-sizing: border-box;
            border: 1px solid #4f87fb;
            border-radius: 20px;
            background-color:#f4f1f3;
        }
        input[type=submit]{
            width: 50%;
            padding: 8px 13px;
            margin: 10px 25%;
            border-radius: 10px;
            border: 2px solid #f4f1f1;
            background-color:#4f87fb;
            color: white;
        }
        input[type=submit]:hover{
            width: 50%;
            padding: 8px 13px;
            margin: 10px 25%;
            border-radius: 10px;
            border: 2px solid #4f87fb;
            background-color:#f2f5e9;
            color: black;
        }
    </style>
    <script src="{{asset('js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
    <title>Spleet.ng - Life Together</title>
</head>
<body>
    
    <!-- SIGNING IN OPTIONS -->
    <div class="formRow">
        <!-- SIGN UP OPTIONS -->
        <div id="signup-options">
            <div class="close"></div>
            <p>sign up</p>
            <div class="facebook">
                <i class="fa fa-facebook-official fa-2x"></i>
                <p><a href="{{route('social.oauth','facebook')}}">Sign up with Facebook</a></p>
            </div>
            <div class="google"><i class="fa fa-google fa-2x"></i><p><a href="{{route('social.oauth','google')}}">Sign up with Google</a></p></div>
            <span>or</span>
            <div class="emailOpt"><i class="fa fa-envelope-o fa-2x"></i><p>Sign up with Email</p></div>
            <p class="agree">By signing up, you accept our Terms of Use and Privacy Policy.</p>
            <div class="baseOpt">
                <p>Already have a Spleet account?</p>
                <input type="submit" class="signin" value="sign in" />
            </div>
        </div> 
        <!-- sign in -->
        <div id="signin-option">                
            <div class="close"></div>
            <p>sign in</p>
            <div class="socialmedia-option">
                <div class="facebookOpt">
                    <i class="fa fa-facebook-official fa-2x"></i>
                    <p><a href="{{route('social.oauth','facebook')}}">with Facebook</a></p>
                </div>
                <div class="googleOpt">
                    <i class="fa fa-google fa-2x"></i>
                    <p><a href="{{route('social.oauth','google')}}">with Google</a></p>
                </div>
            </div>
            <span>or with email</span>
            <form method="post" action="{{route('tenant.login.submit')}}">
                {{ csrf_field() }}
                <input type="text" hidden="hidden" name="route" value="/spaces">
                <div class="email">
                    <input type="email" name="email" placeholder="Enter your Email Address" />
                </div>
                <div class="password">
                    <input type="password" name="password" placeholder="Password" />
                </div>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="rememberme" /><p>Keep me signed in</p>
                <div class="submitBtn">
                    <input type="submit" class="submit" value="sign in" />
                </div>
            </form>
            <p>I forgot my password</p>       
            <div class="base">
                <p>Don't have a Spleet account?</p>
                <input type="submit" id="signupOpts" value="sign up" />
            </div>         
        </div>
        <!-- JOINING SPLEET -->
        <div id="register-option">                
            <div class="close"></div>
            <p>join spleet</p>
            <form method="post" action="{{route('tenant.signup.submit')}}">
                {{ csrf_field() }}
                <input type="" hidden="hidden" name="route" value="/spaces">
                <div>
                    <input type="email" name="email" value="" placeholder="Email Address">
                </div>
                <div>
                    <input type="password" name="password" value="" placeholder="Pasword">
                </div>
                <div>
                    <input type="submit" name="submit" value="sign up">
                </div>
            </form>
            <p>By signing up, you accept our <span>Terms of Use</span> and <span>Privacy Policy.</span></p>
            <div class="baseOpt">
                <p>Already have a Spleet account?</p>
                <input type="submit" class="signin submit" value="sign in" />
            </div>
        </div>
    </div>
    @if ($errors->any())
        <!-- POP-UP FOR ERRORS -->
    <div id="pop-up" style="background: red" class="active">
        <div class="errorMsg"><p style="color: white">Incorrect Username/Password combination</p></div>
        <div class="closeIcon"><i class="fa fa-close fa-2x"></i></div>
    </div>
    @endif

    <div id="searchWrapper">
        <!-- NAVIGATION RULE -->
        <div id="navigation">
            <div class="companyLogo">
                <a href="{{route('index')}}">
                    <img src="{{asset('assets/icons/spleet-2-2@2x.png')}}" alt="Spleet-Ng" />
                </a>
            </div>
            <div class="navigationLinks" id="nav">
                <ul>                
                    @auth
                    <li><a class="mySpleet">My Spleet</a>
                        <ul class="menulist">                            
                            <li><a href="{{route('user.bookmarks')}}">Bookmarks</a></li>
                            <li><a href="{{route('payment.history')}}">Payment History</a></li>
                            <li><a href="{{route('tenant.logout')}}" class="signout">Logout</a></li>
                        </ul>
                    </li>                 
                    @else                        
                        <li><a href="#" class="signin">Sign In</a></li>
                        <li class="noHover">
                            <div class="signupBtn">
                                <a href="#">Sign Up</a>
                            </div>
                        </li>
                    @endauth
                </ul>
            </div>
            <!-- hamburger -->
            <div class="menu-bar">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>

        <!-- SEARCH FORM -->
        <div id="formRow">
             <form method="get" class="SS" name="q" action="{{route('search')}}">
                {{csrf_field()}} 
                <div class="location">
                    <p>location</p>
                    <div id="location">
                        <span id="location">location</span>
                        <input id="location" name="location" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            @foreach ($locations as $location)
                            <li><p>{{ $location->area }}</p></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
                <div class="duration">
                    <p>duration</p>
                    <div id="duration">
                        <span id="duration">Duration</span>
                        <input id="duration" name="duration" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Less than a month</p></li>
                            <li><p>More than a month</p></li>
                        </ul>
                    </div>
                </div>
                <div class="min-price">
                    <p>min. price</p>
                    <div id="min-price">
                        <input id="min-price" name="minprice" type="number" value="" placeholder="60000"/>
                    </div>
                </div>
                <div class="max-price"> 
                    <p>max. price</p>
                    <div id="max-price">
                        <input id="max-price" name="maxprice" type="number" value="" placeholder="100000"/>
                    </div>   
                </div>
                <div class="guard">
                    <p>guard</p>
                    <div id="guard">
                        <span id="guard">Select</span>
                        <input id="guard" name="guard" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Yes</p></li>
                            <li><p>No</p></li>
                        </ul>
                    </div>
                </div>
                <div class="type">
                    <p>type</p>
                    <div id="type">
                        <span id="type">Select</span>
                        <input id="type" name="spacetype" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Single Space</p></li>
                            <li><p>Multi Space</p></li>
                        </ul>
                    </div>
                </div>
                <div class="parking-space">
                    <p>parking space</p>
                    <div id="parking-space">
                        <span id="parking-space">Select</span>
                        <input id="parking-space" name="parkingspace" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Yes</p></li>
                            <li><p>No</p></li>
                        </ul>
                    </div>
                </div>
                <div class="fence">
                    <p>fenced</p>
                    <div id="fence">
                        <span id="fence">Select</span>
                        <input id="fence" name="fence" hidden="hidden" type="text" value="" />
                        <ul class="searchList">
                            <li><p>Yes</p></li>
                            <li><p>No</p></li>
                        </ul>
                    </div>             
                </div>
                <div class="button">
                    <input type="submit" value="search" />
                </div>
            </form>
        </div>

        <!-- RESULT COLUMN -->
        
    @if (count($spaces) > 0)
        <section class="myspaces">
            @include('searchresults.searchresult')
        </section>
    @endif

    </div>
    <script src="{{asset('js/main.js')}}" type="text/javascript"></script>
    <script type="text/javascript">

        // $(function() {
        //     $('body').on('click', '.pagination a', function(e) {
        //         e.preventDefault();

        //         $('#load a').css('color', '#dfecf6');
        //         $('body').append('<img style="position: absolute; left: 0; right: top: 0; z-index: 100000;" src="{{asset('/storage/Spinner.gif')}}" />');

        //         var url = $(this).attr('href');
        //         getArticles(url);
        //         window.history.pushState("", "", url);
        //     });

        //     function getArticles(url) {
        //         $.ajax({
        //             url : url
        //         }).done(function (data) {
        //             $('.myspaces').html(data);
        //         }).fail(function () {
        //             alert('Spaces could not be loaded.');
        //         });
        //     }
        // });
    </script>
</body>
</html>