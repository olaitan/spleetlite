<div class="bookmarkResults">           
            
             @for ($x = 0; $x < $bookmarks->count(); $x++)
                
            <div class="search-result">
                <div class="space-images">
                    <div class="leftbtn">
                        <!-- <a href="" class="left"></a> -->
                    </div>
                    <div class="rightbtn">
                        <!-- <a href="" class="right"></a> -->
                    </div>
                    @for($t=0;$t<count($bookmarks[$x]->space->spacegalleries()->where('resolution',1)->get());$t++)
                        @if($t==0)
                            <img src="../storage/{{$bookmarks[$x]->space->spacegalleries()->where('resolution',1)->get()[$t]->spacepic}}" class="active" alt="image" srcset="">
                        @else
                            <img src="../storage/{{$bookmarks[$x]->space->spacegalleries()->where('resolution',1)->get()[$t]->spacepic}}" alt="image" srcset="">
                        @endif
                        
                    @endfor
                    
                </div>                
                <div class="space-details" onclick="location.href='{{route('space',$bookmarks[$x]->space->id)}}'">
                    <p class="space-name">{{$bookmarks[$x]->space->name}}</p>
                    <p class="address">{{$bookmarks[$x]->space->location->area}}, {{$bookmarks[$x]->space->location->city}}</p>
                    <p class="description">{{$bookmarks[$x]->space->shortdescription}}</p>
                    <p class="amount">NGN {{number_format($bookmarks[$x]->space->price,0,'.',',')}} per {{$bookmarks[$x]->space->paymentfrequency}}</p>
                </div>
                <div class="space-rating">
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star-half-empty fa-1x" aria-hidden="true"></i>
                    <!-- <img src="fa fa-star fa-2x" alt="" /> -->
                    <p>64 rating</p>
                </div>
            </div>
            @endfor
        </div>

        <!-- PAGINATION ROW -->
        {{ $bookmarks->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}