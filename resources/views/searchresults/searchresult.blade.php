<div id="search-query-result">
           <!--<div class="sort">
                <p>Sort by</p>
                <div id="sort-by">
                    <span>average rating</span>
                    <ul class="sortList">
                        <li><p>Lowest Price First</p></li>
                        <li><p>Highest Price First</p></li>
                    </ul>
                </div> 

            </div> -->            
            
            @if($similar==0)
                @for ($x = 0; $x < $spaces->count(); $x++)
                
            <div class="search-result">
                <div class="space-images">
                    <div class="leftbtn">
                        <!-- <a href="" class="left"></a> -->
                    </div>
                    <div class="rightbtn">
                        <!-- <a href="" class="right"></a> -->
                    </div>
                    @for($t=0;$t<count($spaces[$x]->spacegalleries()->where('resolution',1)->get());$t++)
                        @if($t==0)
                            <img src="../storage/{{$spaces[$x]->spacegalleries()->where('resolution',1)->get()[$t]->spacepic}}" class="active" alt="image" srcset="">
                        @else
                            <img src="../storage/{{$spaces[$x]->spacegalleries()->where('resolution',1)->get()[$t]->spacepic}}" alt="image" srcset="">
                        @endif
                        
                    @endfor
                    
                    
                </div>                
                <div class="space-details" onclick="location.href='{{route('space',$spaces[$x]->id)}}'">
                    <p class="space-name">{{$spaces[$x]->name}}</p>
                    <p class="address">{{$spaces[$x]->location->area}}, {{$spaces[$x]->location->city}}</p>
                    
                    <p class="description">{{$spaces[$x]->shortdescription}}</p>
                    <p class="amount">NGN {{number_format($spaces[$x]->price,0,'.',',')}} per {{$spaces[$x]->paymentfrequency}}</p>
                </div>
                <div class="space-rating">
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star-half-empty fa-1x" aria-hidden="true"></i>
                    <!-- <img src="fa fa-star fa-2x" alt="" /> -->
                    <p>64 rating</p>
                </div>
            </div>
            @endfor
            @else
            <div id="sregform" style="width:65%;margin: auto;background-color: #4f87fb;padding: 15px;">
                <span style="margin-left:96%;color: white;cursor: pointer;font-size: 22px" onclick="document.getElementById('sregform').style.display='none'">&times;</span>
                <h3 style="text-align: center;color: white">No Space Available That Matches Your Criteria</h3>
                <form action="{{route('space.interest')}}" method="post">
                    {{csrf_field()}}
                    <h5 style="text-align: center;color: white">kindly enter your area of interest and let us get it for you</h5>
                    
                        <input type="tel" required="required" name="phone" placeholder="Mobile (080000XXXXX)">
                    
                    
                        <input type="text" required="required" name="location" placeholder="Location (Banana Island)">
                        
                    
                    
                        <select name="duration">
                            <option value="_blank">select duration</option>
                            <option value="night">Per night</option>
                            <option value="month">Per month</option>
                        </select>
                    
                    
                        <select name="spacetype">
                            <option value="_blank">select type of space</option>
                            <option value="single">Single Space</option>
                            <option value="multi">Multi Space</option>
                        </select>
                    
                    {{-- <div>
                        
                        <input type="text" name="price" placeholder="Price(NGN) 20000">
                    </div> --}}
                    <input type="submit" name="submit">
                </form>
            </div>
                @if (session('status'))
                <div class="alert alert-info fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss='alert' aria-label='close'>&times;</a>
                    {{ session('status') }}
                </div>
                <script type="text/javascript">
                    document.getElementById('sregform').style.display='none';
                </script>
                @endif
                <br>
                <h4>Similar Spaces</h4>
                @for ($x = 0; $x < $spaces->count(); $x++)
                
            <div class="search-result">
                <div class="space-images">
                    <div class="leftbtn">
                        <!-- <a href="" class="left"></a> -->
                    </div>
                    <div class="rightbtn">
                        <!-- <a href="" class="right"></a> -->
                    </div>
                    <img src="../storage/{{$spaces[$x]->spacegalleries()->where('resolution',1)->get()[0]->spacepic}}" class="active" alt="image" srcset="">
                    <img src="../storage/{{$spaces[$x]->spacegalleries()->where('resolution',1)->get()[0]->spacepic}}" alt="image" srcset="">                    
                    <img src="../storage/{{$spaces[$x]->spacegalleries()->where('resolution',1)->get()[0]->spacepic}}" alt="image" srcset="">
                </div>                
                <div class="space-details" onclick="location.href='{{route('space',$spaces[$x]->id)}}'">
                    <p class="space-name">{{$spaces[$x]->name}}</p>
                    <p class="address">{{$spaces[$x]->location->area}}, {{$spaces[$x]->location->city}}</p>
                    <p class="secondary-address">2nd Line of Address</p>
                    <p class="description">{{$spaces[$x]->shortdescription}}</p>
                    <p class="amount">NGN {{$spaces[$x]->price}} per {{$spaces[$x]->paymentfrequency}}</p>
                </div>
                <div class="space-rating">
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star fa-1x" aria-hidden="true"></i>
                    <i class="fa fa-star-half-empty fa-1x" aria-hidden="true"></i>
                    <!-- <img src="fa fa-star fa-2x" alt="" /> -->
                    <p>64 rating</p>
                </div>
            </div>
            @endfor
            @endif

             
        </div>

        <!-- PAGINATION ROW -->
        {{ $spaces->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}