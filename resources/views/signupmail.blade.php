<h1>Hi, {{ $potentialhomeowner->name }}</h1>
<h3>Sending Mail from Spleet.ng For Registration of Space</h3>
<p>We gladly receive your request to add your space to the spleet platform. It is our duty to inform you that this is the first step in a relatively quick process that will hopefully result in your space being put on the spleet platform.
</p>
<p>Spleet has to inspect and view your space to make sure that it is up to our standards before we can place it on our platform, as such a spleet operative will be in touch with you either by phone or by email to arrange a time for inspection.
	Thank You,
</p>

<p><u>Details of space</u></p>
<p>Address: {{$potentialhomeowner->address}}</p>
<p>Mobile: {{$potentialhomeowner->mobile}}</p>
<hr>
<h4>Spleet Admin</h4>