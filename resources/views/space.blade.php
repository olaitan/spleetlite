<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- STYLESHEET -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/space.css') }}" />
    
    <!-- SCRIPTS -->
    <script type="text/javascript" src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <title>Spleet.ng - Life Together</title>
</head>
<body>
    <div class="formRow" id="formRow">
        <!-- SIGN UP OPTIONS -->
        <div id="signup-options">
            <div class="close"></div>
            <p>sign up</p>
            <div class="facebook">
                <i class="fa fa-facebook-official fa-2x"></i>
                <p><a href="{{route('social.oauth','facebook')}}">Sign up with Facebook</a></p>
            </div>
            <div class="google"><i class="fa fa-google fa-2x"></i><p><a href="{{route('social.oauth','google')}}">Sign up with Google</a></p></div>
            <span>or</span>
            <div class="emailOpt"><i class="fa fa-envelope-o fa-2x"></i><p>Sign up with Email</p></div>
            <p class="agree">By signing up, you accept our Terms of Use and Privacy Policy.</p>
            <div class="baseOpt">
                <p>Already have a Spleet account?</p>
                <input type="submit" class="signin" value="sign in" />
            </div>
        </div> 
    
        <!-- SIGNING IN OPTIONS -->
        <div id="signin-option">                
            <div class="close"></div>
            <p>sign in</p>
            <div class="socialmedia-option">
                <div class="facebookOpt">
                    <i class="fa fa-facebook-official fa-2x"></i>
                    <p><a href="{{route('social.oauth','facebook')}}">with Facebook</a></p>
                </div>
                <div class="googleOpt">
                    <i class="fa fa-google fa-2x"></i>
                    <p><a href="{{route('social.oauth','google')}}">with Google</a></p>
                </div>
            </div>
            <span>or with email</span>
            <form method="post" action="{{route('tenant.login.submit')}}">
                {{ csrf_field() }}
                <input type="text" hidden="hidden" name="route" value="{{route('space',$space->id)}}">
                <div class="email">
                    <input type="email" name="email" placeholder="Enter your Email Address" />
                </div>
                <div class="password">
                    <input type="password" name="password" placeholder="Password" />
                </div>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="rememberme" /><p>Keep me signed in</p>
                <div class="submitBtn">
                    <input type="submit" class="submit" value="sign in" />
                </div>
            </form>
            <p>I forgot my password</p>       
            <div class="base">
                <p>Don't have a Spleet account?</p>
                <input type="submit" id="signupOpts" value="sign up" />
            </div>         
        </div>
    
        <!-- JOINING SPLEET -->
        <div id="register-option">                
            <div class="close"></div>
            <p>join spleet</p>
            <form method="post" action="{{route('tenant.signup.submit')}}">
                {{ csrf_field() }}
                <input type="" hidden="hidden" name="route" value="{{route('space',$space->id)}}">
                <div>
                    <input type="email" name="email" value="" placeholder="Email Address">
                </div>
                <div>
                    <input type="password" name="password" value="" placeholder="Pasword">
                </div>
                <div>
                    <input type="submit" name="submit" value="sign up">
                </div>
            </form>
            <p>By signing up, you accept our <span>Terms of Use</span> and <span>Privacy Policy.</span></p>
            <div class="baseOpt">
                <p>Already have a Spleet account?</p>
                <input type="submit" class="signin submit" value="sign in" />
            </div>
        </div>
    </div> 
    <!-- POP-UP FOR ERRORS -->
    
    <div id="imageWrapper">
        <div class="leftButton"></div>
        <div class="rightButton"></div>
        @for($t=0;$t<count($space->spacegalleries()->where('resolution',1)->get());$t++)
            @if($t==0)
                <img src="../storage/{{$space->spacegalleries()->where('resolution',1)->get()[$t]->spacepic}}" class="active" alt="image" srcset="">
            @else
                <img src="../storage/{{$space->spacegalleries()->where('resolution',1)->get()[$t]->spacepic}}" alt="image" srcset="">
            @endif
                        
        @endfor        
       
    </div>

    <div class="contentWrap">
        @if ($errors->any())
        <!-- POP-UP FOR ERRORS -->
    <div id="pop-up" style="background: red" class="active">
        <div class="errorMsg"><p style="color: white">Incorrect Username/Password combination</p></div>
        <div class="closeIcon"><i class="fa fa-close fa-2x"></i></div>
    </div>
    @endif
        <!-- NAVIGATION RULE -->
        <div id="navigation">
            <div class="companyLogo">
                <a href="{{route('index')}}">
                    <img src="{{asset('assets/icons/spleet-2-2@2x.png')}}" alt="Spleet-Ng" />
                </a>
            </div>
            <div class="navigationLinks" id="nav">
                <ul>                
                    @auth
                    <li><a class="mySpleet">My Spleet</a>
                        <ul class="menulist">                            
                            <li><a href="{{route('user.bookmarks')}}">Bookmarks</a></li>
                            <li><a href="{{route('payment.history')}}">Payment History</a></li>
                            <li><a href="{{route('tenant.logout')}}" class="signout">Logout</a></li>
                        </ul>
                    </li>                  
                    @else                        
                        <li><a href="#" class="signin">Sign In</a></li>
                        <li class="noHover">
                            <div class="signupBtn">
                                <a href="#">Sign Up</a>
                            </div>
                        </li>
                    @endauth
                </ul>
            </div>
            <!-- hamburger -->
            <div class="menu-bar">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>

        <!-- SIDEBAR RULE -->
        <form >
            <script src="https://js.paystack.co/v1/inline.js"></script>
            <div id="sidebar">
                <div class="sidebar-content">
                    <p>average rating</p>
                    <p>4.5<span>/5</span></p>
                    <p>rent</p>
                    <p>{{number_format($space->price,0,'.',',')}}</p>
                    <span>per {{$space->paymentfrequency}}</span>
                </div>
                @if ($space->notavailable==0)
                        @auth
                                    <div class="sidebar-buttons">
                            <div class="check-in">
                                <p onclick="payWithPaystack()">
                                    Check in
                                </p>
                            </div>
                            @if ($space->bookmarks()->where([['tenantid', Auth::id()],['unbookmark', 0],])->count()!=0)
                            <div class="bookmark"  onclick="settingbookmark({{Auth::id()}},{{$space->id}})">
                                <p id="settingbookmark">bookmarked</p>
                            </div>
                            @else
                            <div class="bookmark" onclick="settingbookmark({{Auth::id()}},{{$space->id}})">
                                <p id="settingbookmark">bookmark</p>
                            </div>
                            @endif
                            
                        </div>                 
                        @else
                        <div class="sidebar-buttons">
                            <div class="check-in">
                                <p onclick="payWithPaystack()">
                                    Sign in to Check in
                                </p>
                            </div>
                            <div class="bookmark" onclick="alert('Please sign in / sign up to bookmark a space, Thank you.')">
                                <p>bookmark</p>
                            </div>
                        </div>
                            
                        @endauth 
                            
                @else
                    <div>
                        <h1 style="text-align: center;margin: 5px">Currently In Use</h1>
                    </div> 
                @endif
                
            </div>
        </form>
    </div>

    <div id="space-contents">
        <!-- this the main page showing the space overview -->
        <div id="overview">
            <p>{{$space->name}}</p>
        <p>{{$space->location->area}}, {{$space->location->city}}</p>
    
            <div class="spaceAmenities">
                
            @if($space->amenities->isEmpty())
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/bedroom@2x.png')}}')"></div>
                <p>0 bedroom</p>
            </div>
                <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/living-room@2x.png')}}')"></div>
                <p>0 living room</p>
            </div>
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/kitchen@2x.png')}}')"></div>
                <p>0 kitchen</p>
            </div>
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/bathroom@2x.png')}}')"></div>
                <p>0 toilets</p>
            </div>
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/garage@2x.png')}}')"></div>
                <p>0-car garage</p>
            </div>
            @else
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/bedroom@2x.png')}}')"></div>
                <p>{{$space->amenities[0]->pivot->number}} bedroom</p>
            </div>
                <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/living-room@2x.png')}}')"></div>
                <p>{{$space->amenities[1]->pivot->number}} living room</p>
            </div>
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/kitchen@2x.png')}}')"></div>
                <p>{{$space->amenities[2]->pivot->number}} kitchen</p>
            </div>
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/bathroom@2x.png')}}')"></div>
                <p> {{$space->amenities[3]->pivot->number}} toilets</p>
            </div>
            <div class="amenity">
                <div class="amenityImage" style="background-image: url('{{asset('assets/icons/garage@2x.png')}}')"></div>
                <p>{{$space->amenities[4]->pivot->number}}-car garage</p>
            </div>
            @endif
            
            </div>            
        </div>

        <!-- THE VIEW SECTIONS -->
        <div id="views">
            <ul class="tabs">
                <li><h4 id="view" class="active">overview</h4></li>
                <li><h4 id="inspection">inspection</h4></li>
                <li><h4 id="review">review (64)</h4></li>
            </ul>
            <div class="tab-contents">
                <div id="view" class="active">
                    <p>about this space</p>
                    <p>{{$space->shortdescription}}</p>
                </div>
                <div id="inspection">
                    <p>open for viewing</p>
                    <div class="row">
                        <div class="date">
                            <p>thursday 19 dec</p>
                        </div>
                        <div class="time">
                            <p>03:00 - 05:39pm</p>
                        </div>
                        <div class="button">                            
                            <input type="hidden" name="rsvp">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="date">
                            <p>thursday 19 dec</p>
                        </div>
                        <div class="time">
                            <p>03:00 - 05:39pm</p>
                        </div>
                        <div class="button">
                            <input type="hidden" name="rsvp">
                        </div>
                    </div>
                    <div class="row">
                        <div class="date">
                            <p>thursday 19 dec</p>
                        </div>
                        <div class="time">
                            <p>03:00 - 05:39pm</p>
                        </div>
                        <div class="button">                            
                            <input type="hidden" name="rsvp">
                        </div>
                    </div>
                    <div class="row">
                        <div class="date">
                            <p>thursday 19 dec</p>
                        </div>
                        <div class="time">
                            <p>03:00 - 05:39pm</p>
                        </div>
                        <div class="button">
                            <input type="hidden" name="rsvp">   
                        </div>
                    </div>
                    <div class="row">
                        <div class="date">
                            <p>thursday 19 dec</p>
                        </div>
                        <div class="time">
                            <p>03:00 - 05:39pm</p>
                        </div>
                        <div class="button">
                            <input type="hidden" name="rsvp">
                        </div>
                    </div> 
                </div>
                <div id="review">
                    <p>review & rating <span>(from 64 people)</span></p>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis repudiandae praesentium ex magnam quam obcaecati culpa officiis, quidem inventore, mollitia explicabo unde doloribus porro ut accusamus! Blanditiis illo reiciendis alias.</p>
                </div>
            </div>
        </div>

        <!-- SIMILAR OPTIONS -->
        <div id="similar-spaces">
            <p>similar spaces</p>
            <div class="trending-column" onclick="location.href = '{{route('space',$similarspaces[0]->id)}}';">
            <div class="image trendimage"></div>
            <div class="details">
                <p>{{$similarspaces[0]->name}}</p>
                <p>{{$similarspaces[0]->location->area}}, {{$similarspaces[0]->location->city}}</p>
                <p>NGN {{number_format($similarspaces[0]->price,0,'.',',')}}  per {{$similarspaces[0]->paymentfrequency}}</p>
            </div>
        </div>
        <div class="trending-column" onclick="location.href = '{{route('space',$similarspaces[1]->id)}}';">
            <div class="image trendimage"></div>
            <div class="details">
                <p>{{$similarspaces[1]->name}}</p>
                <p>{{$similarspaces[1]->location->area}}, {{$similarspaces[1]->location->city}}</p>
                <p>NGN {{number_format($similarspaces[1]->price,0,'.',',')}} per {{$similarspaces[1]->paymentfrequency}}</p>
            </div>
        </div>
        <div class="trending-column" onclick="location.href = '{{route('space',$similarspaces[2]->id)}}';">
            <div class="image trendimage"></div>
            <div class="details">
                <p>{{$similarspaces[2]->name}}</p>
                <p>{{$similarspaces[2]->location->area}}, {{$similarspaces[2]->location->city}}</p>
                <p>NGN {{number_format($similarspaces[2]->price,0,'.',',')}} per {{$similarspaces[2]->paymentfrequency}}</p>
            </div>
        </div>
        <div class="trending-column" onclick="location.href = '{{route('space',$similarspaces[3]->id)}}';">
            <div class="image trendimage"></div>
            <div class="details">
                <p>{{$similarspaces[3]->name}}</p>
                <p>{{$similarspaces[3]->location->area}}, {{$similarspaces[3]->location->city}}</p>
                <p>NGN {{number_format($similarspaces[3]->price,0,'.',',')}} per {{$similarspaces[3]->paymentfrequency}}</p>
            </div>
        </div>
        </div>
    </div>

    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
    <script>
        var xmlhttp;
        function settingbookmark(tenant,space){
            if (document.getElementById("settingbookmark").innerHTML=="bookmarked") {
                xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function()
              {
              if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    //change to bookmarked
                document.getElementById("settingbookmark").innerHTML=xmlhttp.responseText;
                }
              }
            xmlhttp.open("GET","{{route('user.unsetbookmark')}}?tid="+tenant+"&sid="+space,true);
            xmlhttp.send();
            }
             else {
                xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function()
              {
              if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    //change to bookmarked
                document.getElementById("settingbookmark").innerHTML=xmlhttp.responseText;
                }
              }
            xmlhttp.open("GET","{{route('user.setbookmark')}}?tid="+tenant+"&sid="+space,true);
            xmlhttp.send();
             }
        }
        
            var propImage = [];
            propImage = document.getElementsByClassName("image trendimage");

            propImage[0].style.backgroundImage = "url('../storage/{{$similarspaces[0]->spacegalleries[0]->spacepic}}')";
            propImage[1].style.backgroundImage = "url('../storage/{{$similarspaces[1]->spacegalleries[0]->spacepic}}')";
            propImage[2].style.backgroundImage = "url('../storage/{{$similarspaces[2]->spacegalleries[0]->spacepic}}')";
            propImage[3].style.backgroundImage = "url('../storage/{{$similarspaces[3]->spacegalleries[0]->spacepic}}')";
    </script>
    <script>
        function payWithPaystack(){
            @auth
                var handler = PaystackPop.setup({
                key: 'pk_test_4efc2288b6a51480eb411de037b896c712cb3b0d',
                email: '{{Auth::user()->email}}',
                amount: {{$space->price}}00,
                metadata: {
                    custom_fields: [
                        {
                            display_name: "Name",
                            variable_name: "name",
                            value: "{{Auth::user()->name}}"
                        }
                    ]
                },
                callback: function(response){
                    location.href = '{{route('paystackverification')}}/'+response.reference+'/{{$space->id}}';
                },
                onClose: function(){
                    alert('window closed');
                }
                });
                handler.openIframe();                           
                @else
                alert("Please sign in/ sign up to continue. Thanks");
            @endauth         
        }
    </script>
</body>
</html>