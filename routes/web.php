<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('','HomeController@index')->name('index');

Route::post('/potential/homeowners','HomeOwnerController@register')->name('spaceregister');

Route::get('/space/{id}','SpaceController@index')->name('space');

Route::get('/spaces','SpaceController@spaces')->name('space.search');

Route::get('/spaces/search', 'SpaceController@search')->name('search');

Route::get('/space/location/{name}','SpaceController@location')->name('location');

Route::get('/user/paymenthistory','TenantController@checkpaymenthistory')->name('payment.history');

Route::get('/user/bookmarks','TenantController@checkbookmarks')->name('user.bookmarks');
Route::get('/user/bookmarks/search', 'TenantController@search')->name('user.search');

Route::get('/user/bookmark/set', 'TenantController@setbookmark')->name('user.setbookmark');
Route::get('/user/bookmark/unset', 'TenantController@unsetbookmark')->name('user.unsetbookmark');

Route::post('/spaceinterest', 'SpaceController@registerinterest')->name('space.interest');

//Paystack verification
Route::get('/paystack/verification/{ref?}/{spaceid?}','PaystackController@verify')->name('paystackverification');

//Mail notification
Route::get('sendbasicemail','MailController@basic_email');
Route::get('sendhtmlemail/{email}/{name}/{usertype}/{subject}/{address}/{mobile}','MailController@html_email');
Route::get('sendattachmentemail','MailController@attachment_email');

//Email Auth
Route::post('/signup', 'TenantRegisterController@create')->name('tenant.signup.submit');
Route::post('/login', 'Auth\TenantLoginController@login')->name('tenant.login.submit');
Route::get('logout/', 'Auth\TenantLoginController@logout')->name('tenant.logout');

//Social and normal Auth
Route::get('auth/social', 'Auth\SocialAuthController@show')->name('social.login');
Route::get('oauth/{driver}', 'Auth\SocialAuthController@redirectToProvider')->name('social.oauth');
Route::get('oauth/{driver}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');
