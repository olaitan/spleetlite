-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2018 at 01:36 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spleet_test_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE `amenities` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `amenityicon` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`id`, `name`, `amenityicon`) VALUES
(1, 'Bedroom', ''),
(2, 'Living Room', ''),
(3, 'Kitchen', ''),
(4, 'Toilet', ''),
(5, 'Car Garage', '');

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `id` bigint(20) NOT NULL,
  `tenantid` bigint(20) NOT NULL,
  `spaceid` bigint(20) NOT NULL,
  `unbookmark` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmarks`
--

INSERT INTO `bookmarks` (`id`, `tenantid`, `spaceid`, `unbookmark`) VALUES
(1, 14, 1, 0),
(2, 14, 5, 1),
(3, 14, 6, 1),
(4, 11, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `homeowners`
--

CREATE TABLE `homeowners` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `profilepic` varchar(200) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `password` varchar(100) NOT NULL,
  `paystackid` int(11) NOT NULL,
  `remember_token` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeowners`
--

INSERT INTO `homeowners` (`id`, `email`, `firstname`, `middlename`, `lastname`, `mobile`, `profilepic`, `gender`, `address`, `dob`, `password`, `paystackid`, `remember_token`) VALUES
(1, 'funke.Aji@gmail.com', 'Ajibola', 'Funmilola', 'Funke', '2348172565983', '', 'female', '', '0000-00-00', 'funke12', 0, NULL),
(2, 'ty.py@gmail.com', 'Ty', 'John', 'Pelumi', '', '', 'male', '', '0000-00-00', 'typy123', 0, NULL),
(3, 'umoh.chinedu@gmail.com', 'Umoh', 'Christian', 'Chinedu', '', '', 'male', '', '0000-00-00', 'umoh123', 0, NULL),
(4, 'kafi.Tamia@gmail.com', 'Kafi', 'Jane', 'Tamia', '', '', 'female', '', '0000-00-00', 'tamia123', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `city` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `longitude`, `latitude`, `city`, `area`, `address`) VALUES
(1, 100, 105, 'Lagos', 'Ikoyi', '12, Awolowo Road '),
(2, 15, 200, 'Lagos', 'Oworoshoki', '12,shomolu street'),
(3, 89, 75, 'Lagos', 'Shomolu', '15, Evans Street'),
(4, 206, 150, 'Abuja', 'Garki', '24, Aso Rock Road'),
(5, 50, 290, 'Lagos', 'Lekki', '12, Bishop street, lekki phase 1'),
(6, 80, 300, 'Lagos', 'Ikoyi', '10, Awolowo road'),
(7, 20, 85.15, 'Lagos', 'Gbagada', '24, Opebi street'),
(8, 89, 210, 'Lagos', 'Victoria Island', '7,Mcreynold street'),
(9, 80, 69, 'Lagos', 'Badagry', '24, Asamoah street');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `potentialhomeowners`
--

CREATE TABLE `potentialhomeowners` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `time` varchar(15) NOT NULL,
  `date` varchar(15) NOT NULL,
  `photodirectory` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `potentialhomeowners`
--

INSERT INTO `potentialhomeowners` (`id`, `name`, `email`, `mobile`, `address`, `time`, `date`, `photodirectory`) VALUES
(1, 'olaitan adetayo', 'olaitan.adetayo@gmail.com', '08057234927', '24,Jaiyeoba street Ijeododo, ijegun, ikotun, lagos', '08:35', '2018-05-01', 'olaitan.adetayo@gmail.com'),
(2, 'Israel Akpan', 'israelakpan296@gmail.com', '08132218543', 'Fuji House Of Peace', '08:25', '2018-01-20', 'israelakpan296@gmail.com'),
(3, 'Israel Akpan', 'israelakpan296@gmail.com', '08132218543', 'Fuji House Of Peace', '09:36', '2018-01-25', 'israelakpan296@gmail.com'),
(4, 'Israel Akpan', 'israelakpan296@gmail.com', '08132218543', 'Fuji House Of Peace', '09:36', '2018-01-25', 'israelakpan296@gmail.com'),
(5, 'Israel Akpan', 'israelakpan296@gmail.com', '08132218543', 'Fuji House Of Peace', '09:36', '2018-01-25', 'israelakpan296@gmail.com'),
(6, 'Israel Akpan', 'israelakpan296@gmail.com', '08132218543', 'Fuji House Of Peace', '09:36', '2018-01-25', 'israelakpan296@gmail.com'),
(7, 'Olaitan Adetayo', 'olaitan.adetayo@gmail.com', '8057234927', 'Akure', '05:30', '2018-12-02', 'olaitan.adetayo@gmail.com'),
(8, 'Olaitan Adetayo', 'olaitan.adetayo@gmail.com', '8057234927', 'Akure', '02:15', '2018-11-02', 'olaitan.adetayo@gmail.com'),
(9, 'Olaitan Adetayo', 'olaitan.adetayo@gmail.com', '8057234927', 'Akure', '02:15', '2018-11-02', 'olaitan.adetayo@gmail.com'),
(10, 'olaitan adetayo', 'olaitan.adetayo@gmail.com', '07062639037', '24,Jaiyeoba street Ijeododo, ijegun, ikotun, lagos', '02:15', '2018-02-05', 'olaitan.adetayo@gmail.com'),
(11, 'Olaitan Adetayo', 'olaitan.adetayo@gmail.com', '8057234927', 'Akure', '02:15', '2018-08-25', 'olaitan.adetayo@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `spaceid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spaceamenities`
--

CREATE TABLE `spaceamenities` (
  `id` int(11) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `amenityid` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spaceamenities`
--

INSERT INTO `spaceamenities` (`id`, `spaceid`, `amenityid`, `number`) VALUES
(1, 1, 1, 2),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 4, 2),
(5, 1, 5, 3),
(6, 2, 1, 1),
(7, 2, 2, 1),
(8, 2, 3, 1),
(9, 2, 4, 1),
(10, 2, 5, 0),
(11, 3, 1, 1),
(12, 3, 2, 1),
(13, 3, 3, 1),
(14, 3, 4, 1),
(15, 3, 5, 3),
(16, 4, 1, 2),
(17, 4, 2, 1),
(18, 4, 3, 1),
(19, 4, 4, 1),
(20, 4, 5, 1),
(21, 5, 1, 1),
(22, 5, 2, 1),
(23, 5, 3, 1),
(24, 5, 4, 1),
(25, 5, 5, 1),
(26, 6, 1, 1),
(27, 6, 2, 1),
(28, 6, 3, 1),
(29, 6, 4, 1),
(30, 6, 5, 0),
(31, 7, 1, 2),
(32, 7, 2, 1),
(33, 7, 3, 1),
(34, 7, 4, 2),
(35, 7, 5, 0),
(36, 8, 1, 3),
(37, 8, 2, 2),
(38, 8, 3, 1),
(39, 8, 4, 2),
(40, 8, 5, 3),
(41, 9, 1, 2),
(42, 9, 2, 1),
(43, 9, 3, 1),
(44, 9, 4, 2),
(45, 9, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `spacebookingtransactions`
--

CREATE TABLE `spacebookingtransactions` (
  `id` int(11) NOT NULL,
  `tenantid` int(11) NOT NULL,
  `bookingdate` datetime NOT NULL,
  `paymentstatus` varchar(200) NOT NULL,
  `paymentreference` varchar(200) NOT NULL,
  `amount` varchar(200) NOT NULL,
  `isautomaticallyrecurring` int(11) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `authorizationcode` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacebookingtransactions`
--

INSERT INTO `spacebookingtransactions` (`id`, `tenantid`, `bookingdate`, `paymentstatus`, `paymentreference`, `amount`, `isautomaticallyrecurring`, `spaceid`, `authorizationcode`) VALUES
(1, 14, '2018-01-13 21:01:32', 'success', 'T784161954849345', '60000', 1, 6, 'AUTH_tpgdqcstz8'),
(7, 14, '2018-01-13 22:02:51', 'success', 'T840381124141271', '50000', 1, 2, 'AUTH_be8yaacvaq'),
(8, 11, '2018-01-13 22:08:11', 'success', 'T311887737559042', '4000', 1, 8, 'AUTH_1ggb5konp5');

-- --------------------------------------------------------

--
-- Table structure for table `spacecontracts`
--

CREATE TABLE `spacecontracts` (
  `id` int(11) NOT NULL,
  `startdate` varchar(225) NOT NULL,
  `enddate` varchar(225) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `tenantid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacecontracts`
--

INSERT INTO `spacecontracts` (`id`, `startdate`, `enddate`, `spaceid`, `tenantid`) VALUES
(1, '2018-01-13', '2018-01-13', 6, 11),
(2, '2018-01-13', '2018-01-13', 2, 12),
(3, '2018-01-13', '2018-01-13', 8, 11);

-- --------------------------------------------------------

--
-- Table structure for table `spacegalleries`
--

CREATE TABLE `spacegalleries` (
  `id` int(11) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `spacepic` varchar(200) NOT NULL,
  `resolution` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacegalleries`
--

INSERT INTO `spacegalleries` (`id`, `spaceid`, `spacepic`, `resolution`) VALUES
(1, 1, 'lag.jpg', 1),
(2, 1, 'lag1.jpg', 2),
(3, 2, 'lag_vi.jpg', 1),
(4, 2, 'lag_vi1.jpg', 2),
(5, 3, 'lag_oworo.jpg', 1),
(6, 3, 'lag_oworo1.jpg', 2),
(7, 4, 'lag_lekki.jpg', 1),
(8, 4, 'lag_lekki1.jpg', 2),
(9, 5, 'lag_island.jpg', 1),
(10, 5, 'lag_island1.jpg', 2),
(11, 6, 'lag_ikoyi.jpg', 1),
(12, 6, 'lag_ikoyi1.jpg', 2),
(13, 7, 'lag_ikj.jpg', 1),
(14, 7, 'lag_ikj1.jpg', 2),
(15, 8, 'lag_gbagada.jpg', 1),
(16, 8, 'lag_gbagada1.jpg', 2),
(17, 9, 'lag_badagry.jpg', 1),
(18, 9, 'lag_badagry1.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `spaceinterests`
--

CREATE TABLE `spaceinterests` (
  `id` int(11) NOT NULL,
  `phone` varchar(225) DEFAULT NULL,
  `location` varchar(225) DEFAULT NULL,
  `duration` varchar(225) DEFAULT NULL,
  `spacetype` varchar(225) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spaceinterests`
--

INSERT INTO `spaceinterests` (`id`, `phone`, `location`, `duration`, `spacetype`, `price`, `created_at`, `updated_at`) VALUES
(1, '07062639037', 'Ipaja', 'night', 'single', 1000, '2018-01-20 21:06:15', '2018-01-20 21:06:15'),
(2, '08132218543', 'oworo', 'night', 'single', 1000, '2018-01-20 21:09:05', '2018-01-20 21:09:05');

-- --------------------------------------------------------

--
-- Table structure for table `spaces`
--

CREATE TABLE `spaces` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `shortdescription` text,
  `price` int(11) NOT NULL,
  `paymentfrequency` varchar(100) NOT NULL,
  `homeownerid` int(11) NOT NULL,
  `locationid` int(11) NOT NULL,
  `area` varchar(200) DEFAULT NULL,
  `notavailable` int(11) NOT NULL,
  `guard` tinyint(1) NOT NULL,
  `fence` tinyint(1) NOT NULL,
  `parkingspace` tinyint(1) NOT NULL,
  `spacetypeid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spaces`
--

INSERT INTO `spaces` (`id`, `name`, `shortdescription`, `price`, `paymentfrequency`, `homeownerid`, `locationid`, `area`, `notavailable`, `guard`, `fence`, `parkingspace`, `spacetypeid`) VALUES
(1, 'Beautiful Flat', 'Beuatiful space with luxury.\r\n\r\nBeautiful scence of the lagoon.\r\nGood security.\r\n\r\nMore than enough car spaces.', 5000, 'night', 1, 1, 'Ikoyi', 0, 1, 1, 1, 1),
(2, 'Beautiful Terrain', 'A beautiful space for couples.', 50000, 'month', 1, 2, 'Oworoshoki', 0, 0, 0, 0, 1),
(3, 'Skyline Self Contain', 'Beuatiful space with luxury.\r\nBeautiful scence of the lagoon.\r\nGood security.\r\nMore than enough car spaces.\r\nPrivacy is our top priority', 2500, 'night', 2, 3, 'Shomolu', 0, 1, 1, 1, 1),
(4, '2 Bedroom Flat', 'Beuatiful space with luxury.\r\nGood security.\r\n', 30000, 'month', 3, 4, 'Garki', 0, 1, 0, 0, 2),
(5, '1 Bedroom flat Self Contain', 'Beuatiful space with luxury.\r\nBeautiful scence of the lagoon.\r\nGood security.\r\nMore than enough car spaces.', 2500, 'night', 2, 5, 'Lekki', 0, 1, 0, 1, 1),
(6, 'Beautiful Oceanview Flat', 'Beuatiful space with luxury.\r\nBeautiful Ocean view\r\nPrivacy is our priority with the compound fenced at all sides', 60000, 'month', 1, 6, 'Ikoyi', 0, 0, 1, 0, 1),
(7, '2 Bedroom Flat', 'Beuatiful space with luxury.\r\nGood security.', 5000, 'night', 4, 7, 'Gbagada', 0, 1, 0, 0, 2),
(8, '3 Bedroom Flat', 'Beuatiful space with luxury.\r\nMore than enough car spaces.', 4000, 'night', 3, 8, 'Victoria Island', 0, 0, 0, 1, 2),
(9, '2 Bedroom Flat', 'Beuatiful space with luxury.\r\nBeautiful scence of the lagoon.\r\nGood security.\r\nMore than enough car spaces.\r\nYou are free to use pets.\r\nElectricity is good.', 28500, 'month', 1, 9, 'Badagry', 0, 1, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `spacetype`
--

CREATE TABLE `spacetype` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacetype`
--

INSERT INTO `spacetype` (`id`, `name`) VALUES
(1, 'single space'),
(2, 'multi space');

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `profilepic` varchar(200) DEFAULT NULL,
  `occupation` varchar(100) DEFAULT NULL,
  `gender` varchar(30) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `paystackid` int(11) DEFAULT NULL,
  `remember_token` varchar(150) DEFAULT NULL,
  `access_token` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `provider` varchar(200) DEFAULT NULL,
  `provider_id` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `email`, `firstname`, `middlename`, `lastname`, `mobile`, `profilepic`, `occupation`, `gender`, `dob`, `password`, `paystackid`, `remember_token`, `access_token`, `name`, `provider`, `provider_id`) VALUES
(1, 'funki@gmail.com', 'Steven', 'Jane', 'Funke', '+2348023568941', '', 'Trader', 'Female', '1990-12-05', '', 0, 'WzppBGQB0vZUKxKsJVey4PO7UipLl7416Gf3QL0GXcGRJsUhh1G8cco6Vkli', NULL, NULL, '', ''),
(2, 'ebenuzoh@gmail.com', 'Uzoh', 'Paul', 'Eben', '+2348154268731', '', 'Banker', 'Male', '1985-10-05', 'EbenUzoh123', 0, NULL, NULL, NULL, '', ''),
(3, 'umaina.kida@gmail.com', 'Umaina', 'Kidaffa', 'Scott', '+23415236789', '', 'Lawyer', 'Male', '1985-05-08', '', 0, NULL, NULL, NULL, '', ''),
(4, 'Ola.Toyin@gmail.com', 'Olaiya', 'Titilayo', 'Toyin', '+23451629487', '', 'Doctor', 'Female', '1988-02-09', 'titi123', 0, NULL, NULL, NULL, '', ''),
(5, 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$TIV7Cc8Cta3wPewZ7nQ9aObGsr9tWPOgw.auHvt2Za5RPEgCNF6ZG', NULL, 'IzbpJ4HDTGjdTHD4ytqFtZHYIBoYhtTbhHjsiCkirlm0f32xNJ9tYEgdKG0Q', NULL, NULL, '', ''),
(6, 'newadmin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$iWOCnF.Ja/EbK.bmlqlmbu9IlHdjVJjbBRwF3yAjM.F.Qr/B6/ify', NULL, NULL, NULL, NULL, '', ''),
(7, 'admin_super@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$B5kX77N4wwEW1X8PYvPc6uUSvJ3d68EoYDbgBrXpY44XPE6fO7Mka', NULL, NULL, NULL, NULL, '', ''),
(8, 'omg@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'omg123', NULL, NULL, NULL, NULL, '', ''),
(9, 'dare@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$swTHLZW5QSgd/rxbFJXizeINEwCx.fUy1Ej/BWaBd4ilis8k0U1Pi', NULL, '4CVWH0M0TuXdMRNE4E7Wx7hUtKDZMELwZ5qgJEdXTsNW9t2zV3RQjgIMlNaL', NULL, NULL, '', ''),
(10, 'olaitan_adetayo@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1b9denGsEQDIVPWOKlffnb24hMOP7L2fzRtOGp8mKbl44cnTPwnXqBJvCuAV', 'EAAHkU2kfElMBAIKjAdDfpnEsrwesZBUMsX4zIZAq2nYYdHSdbUHhYV93CNc2Yka0O8E9IQizCJ54Lt2puxy4ZAwjjVdCWQ34ZC3DPL7JBptbP18wrmhZAJSBztoP5b18ZB6R4iFiOcBVCDBAmZACZBfRWLiZCZCAEBazWKrYZBB5r45tQZDZD', 'Olaitan Adetayo', 'facebook', '1576784985742548'),
(11, 'olaitan.adetayo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'FDKtZfrrQrYz8mARXUoTGwzSkoz972iGvylB6xMMgyOvlz0DHkSTDwWTlYn5', 'ya29.GlxHBToBI6m1m8SpwBkV2xhFS2Y_2gDijRCJrXrzKzYgU-R6_j5UdQ_PEAGt7qukXfGx4_2Qv7GIwBdaNbIJvwD9bURoRpL1w5pnOX0CtvJwFAuBbsAgnmwj-CqUVQ', 'olaitan adetayo', 'google', '100570029115294211751'),
(12, 'iamolaitanajibola@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'MtQJ5w0jDDJsf7xN5BzcngpQfe7RB1OsaihneU8Ph2tL69IHmSELemXYx1Ls', 'ya29.GltBBegTso3eqeYBP5mKZYOXkcoNRrGk2iytZdqJVXiXDgKT0r4NuheKRTDCmI63UCFS9kt6MtfKyoinvZNlS0fkXnKDJmGTnrXykEpMt-gMBYuZ-fNB8Mc_WTat', 'Olaitan Adetayo', 'google', '110540068012353107005'),
(13, 'teewhy@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$hbBL0c1JLrnKt2RQuD6nfeDU8GOyzjirvM5YGTQ81GTtyefgVwoRS', NULL, 'pwmxXsSE0GkgYIwZk7qvwENKXEGOO3GKfd2HFG63Qh13uWgakTHSZm22OblY', NULL, NULL, NULL, NULL),
(14, 'ty@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$bW8fd12TncLe9n6DnvBihe1hkyYMjMpfT5Jih.twtO2RWyrbmfNVK', NULL, '273JRCzDwvBiMsAwwtzMvYyTZUcomZJNVJwCyxqlYjTiwn00YQFMtzpzSEle', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', 'Ola.Toyin@gmail.com', 'titi123', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amenities`
--
ALTER TABLE `amenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeowners`
--
ALTER TABLE `homeowners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `potentialhomeowners`
--
ALTER TABLE `potentialhomeowners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spaceamenities`
--
ALTER TABLE `spaceamenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacebookingtransactions`
--
ALTER TABLE `spacebookingtransactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacecontracts`
--
ALTER TABLE `spacecontracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacegalleries`
--
ALTER TABLE `spacegalleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spaceinterests`
--
ALTER TABLE `spaceinterests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spaces`
--
ALTER TABLE `spaces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacetype`
--
ALTER TABLE `spacetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amenities`
--
ALTER TABLE `amenities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `homeowners`
--
ALTER TABLE `homeowners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `potentialhomeowners`
--
ALTER TABLE `potentialhomeowners`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spaceamenities`
--
ALTER TABLE `spaceamenities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `spacebookingtransactions`
--
ALTER TABLE `spacebookingtransactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `spacecontracts`
--
ALTER TABLE `spacecontracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `spacegalleries`
--
ALTER TABLE `spacegalleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `spaceinterests`
--
ALTER TABLE `spaceinterests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `spaces`
--
ALTER TABLE `spaces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `spacetype`
--
ALTER TABLE `spacetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
