-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2018 at 03:46 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spleet_test_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE `amenities` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `amenityicon` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`id`, `name`, `amenityicon`) VALUES
(1, 'Bedroom', ''),
(2, 'Living Room', ''),
(3, 'Kitchen', ''),
(4, 'Toilet', ''),
(5, 'Car Garage', '');

-- --------------------------------------------------------

--
-- Table structure for table `availablelocations`
--

CREATE TABLE `availablelocations` (
  `id` int(11) NOT NULL,
  `area` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `availablelocations`
--

INSERT INTO `availablelocations` (`id`, `area`) VALUES
(1, 'lekki'),
(3, 'gbagada'),
(7, 'omole');

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `id` bigint(20) NOT NULL,
  `tenantid` bigint(20) NOT NULL,
  `spaceid` bigint(20) NOT NULL,
  `unbookmark` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmarks`
--

INSERT INTO `bookmarks` (`id`, `tenantid`, `spaceid`, `unbookmark`) VALUES
(6, 15, 12, 1),
(7, 15, 17, 0);

-- --------------------------------------------------------

--
-- Table structure for table `homeowners`
--

CREATE TABLE `homeowners` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `profilepic` varchar(200) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `password` varchar(100) NOT NULL,
  `paystackid` int(11) NOT NULL,
  `remember_token` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `city` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `longitude`, `latitude`, `city`, `area`, `address`) VALUES
(10, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(11, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(12, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(13, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(14, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(15, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(16, 0, 0, 'lagos', 'lekki', 'freedom way'),
(17, 0, 0, 'lagos', 'lekki', 'freedom way'),
(18, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(19, 0, 0, 'lagos', 'lekki', 'ikate elegushi'),
(20, 0, 0, 'lagos', 'omole', 'omole phase 1'),
(21, 0, 0, 'lagos', 'gbagada', 'gbagada'),
(22, 0, 0, 'lagos', 'gbagada', 'gbagada'),
(23, 0, 0, 'lagos', 'lekki', 'bunmi olowude');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `potentialhomeowners`
--

CREATE TABLE `potentialhomeowners` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `time` varchar(15) NOT NULL,
  `date` varchar(15) NOT NULL,
  `photodirectory` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `spaceid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spaceamenities`
--

CREATE TABLE `spaceamenities` (
  `id` int(11) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `amenityid` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spaceamenities`
--

INSERT INTO `spaceamenities` (`id`, `spaceid`, `amenityid`, `number`) VALUES
(46, 10, 1, 1),
(47, 10, 2, 1),
(48, 10, 3, 1),
(49, 10, 4, 1),
(50, 10, 5, 1),
(51, 11, 1, 1),
(52, 11, 2, 1),
(53, 11, 3, 1),
(54, 11, 4, 1),
(55, 11, 5, 1),
(56, 12, 1, 1),
(57, 12, 2, 1),
(58, 12, 3, 1),
(59, 12, 4, 1),
(60, 12, 5, 1),
(61, 13, 1, 1),
(62, 13, 2, 1),
(63, 14, 3, 1),
(64, 14, 4, 1),
(65, 14, 5, 1),
(66, 15, 1, 1),
(67, 15, 2, 1),
(68, 15, 3, 1),
(69, 15, 4, 1),
(70, 15, 5, 1),
(71, 16, 1, 1),
(72, 16, 2, 1),
(73, 16, 3, 1),
(74, 16, 4, 1),
(75, 16, 5, 1),
(76, 17, 1, 1),
(77, 17, 2, 1),
(78, 17, 3, 1),
(79, 17, 4, 1),
(80, 17, 5, 1),
(81, 18, 1, 1),
(82, 18, 2, 1),
(83, 18, 3, 1),
(84, 18, 4, 1),
(85, 18, 5, 1),
(86, 19, 1, 1),
(87, 19, 2, 1),
(88, 19, 3, 1),
(89, 19, 4, 1),
(90, 19, 5, 1),
(91, 20, 1, 1),
(92, 20, 2, 1),
(93, 20, 3, 1),
(94, 20, 4, 1),
(95, 20, 5, 1),
(96, 21, 1, 1),
(97, 21, 2, 1),
(98, 21, 3, 1),
(99, 21, 4, 1),
(100, 21, 5, 1),
(101, 22, 1, 1),
(102, 22, 2, 1),
(103, 22, 3, 1),
(104, 22, 4, 1),
(105, 22, 5, 1),
(106, 23, 1, 1),
(107, 23, 2, 1),
(108, 23, 3, 1),
(109, 23, 4, 1),
(110, 23, 5, 1),
(111, 13, 3, 1),
(112, 13, 4, 1),
(113, 13, 5, 1),
(114, 14, 1, 1),
(115, 14, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `spacebookingtransactions`
--

CREATE TABLE `spacebookingtransactions` (
  `id` int(11) NOT NULL,
  `tenantid` int(11) NOT NULL,
  `bookingdate` datetime NOT NULL,
  `paymentstatus` varchar(200) NOT NULL,
  `paymentreference` varchar(200) NOT NULL,
  `amount` varchar(200) NOT NULL,
  `isautomaticallyrecurring` int(11) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `authorizationcode` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spacecontracts`
--

CREATE TABLE `spacecontracts` (
  `id` int(11) NOT NULL,
  `startdate` varchar(225) NOT NULL,
  `enddate` varchar(225) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `tenantid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spacegalleries`
--

CREATE TABLE `spacegalleries` (
  `id` int(11) NOT NULL,
  `spaceid` int(11) NOT NULL,
  `spacepic` varchar(200) NOT NULL,
  `resolution` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacegalleries`
--

INSERT INTO `spacegalleries` (`id`, `spaceid`, `spacepic`, `resolution`) VALUES
(19, 10, 'space1/S1.jpg', 1),
(20, 10, 'space1/S2.jpg', 1),
(21, 11, 'space6/S1.jpg', 1),
(22, 11, 'space6/S2.jpg', 1),
(23, 12, 'space5/S1.jpg', 1),
(24, 12, 'space5/S2.jpg', 1),
(25, 13, 'lag_lekki.jpg', 1),
(26, 14, 'lag_lekki.jpg', 1),
(27, 15, 'space3/S1.jpg', 1),
(28, 15, 'space3/S2.jpg', 1),
(29, 15, 'space3/S3.jpg', 1),
(30, 15, 'space3/S4.jpg', 1),
(31, 16, 'space10/S1.jpg', 1),
(32, 17, 'space11/S1.jpg', 1),
(33, 18, 'space8/S1.jpg', 1),
(34, 19, 'space9/S1.jpg', 1),
(35, 20, 'space2/S1.jpg', 1),
(36, 20, 'space2/S2.jpg', 1),
(37, 20, 'space2/S3.jpg', 1),
(38, 20, 'space2/S4.jpg', 1),
(39, 20, 'space2/S5.jpg', 1),
(40, 21, 'lag_lekki.jpg', 1),
(41, 22, 'lag_lekki.jpg', 1),
(42, 23, 'space7/S1.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `spaceinterests`
--

CREATE TABLE `spaceinterests` (
  `id` int(11) NOT NULL,
  `phone` varchar(225) DEFAULT NULL,
  `location` varchar(225) DEFAULT NULL,
  `duration` varchar(225) DEFAULT NULL,
  `spacetype` varchar(225) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spaces`
--

CREATE TABLE `spaces` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `shortdescription` text,
  `price` int(11) NOT NULL,
  `paymentfrequency` varchar(100) NOT NULL,
  `homeownerid` int(11) NOT NULL,
  `locationid` int(11) NOT NULL,
  `area` varchar(200) DEFAULT NULL,
  `notavailable` int(11) NOT NULL,
  `guard` tinyint(1) NOT NULL,
  `fence` tinyint(1) NOT NULL,
  `parkingspace` tinyint(1) NOT NULL,
  `spacetypeid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spaces`
--

INSERT INTO `spaces` (`id`, `name`, `shortdescription`, `price`, `paymentfrequency`, `homeownerid`, `locationid`, `area`, `notavailable`, `guard`, `fence`, `parkingspace`, `spacetypeid`) VALUES
(10, 'Beautiful Apartment', 'A Beautiful Apartment', 115000, 'month', 1, 10, 'lekki', 0, 1, 1, 1, 2),
(11, 'Beautiful Apartment', 'A Beautiful Apartment', 135000, 'month', 1, 11, 'lekki', 0, 1, 1, 1, 2),
(12, 'Beautiful Apartment', 'A Beautiful Apartment', 160000, 'month', 1, 12, 'lekki', 0, 1, 1, 1, 2),
(13, 'Bella Apartment', 'A Beautiful Apartment', 65000, 'night', 1, 13, 'lekki', 0, 1, 1, 1, 2),
(14, 'Bella 3 Rooms Apartment', 'A Beautiful Apartment', 75000, 'night', 1, 14, 'lekki', 0, 1, 1, 1, 2),
(15, 'Otaiku 3 Rooms Apartment', 'A Beautiful Apartment', 65000, 'month', 1, 15, 'lekki', 0, 1, 1, 1, 2),
(16, 'Beautiful Apartment', 'A Beautiful Apartment', 145000, 'month', 1, 16, 'lekki', 0, 1, 1, 1, 2),
(17, 'Beautiful Apartment', 'A Beautiful Apartment', 165000, 'month', 1, 17, 'lekki', 0, 1, 1, 1, 2),
(18, 'Villa 33 Apartment', 'A Beautiful Apartment', 20000, 'night', 1, 18, 'lekki', 0, 1, 1, 1, 1),
(19, 'Beautiful Apartment', 'A Beautiful Apartment', 45000, 'night', 1, 19, 'lekki', 0, 1, 1, 1, 1),
(20, 'Beautiful 2 Rooms Apartment', 'A Beautiful Apartment', 65000, 'night', 1, 20, 'omole', 0, 1, 1, 1, 2),
(21, 'Beautiful Apartment', 'A Beautiful Apartment', 12500, 'night', 1, 21, 'gbagada', 0, 1, 1, 1, 1),
(22, 'Beautiful Apartment', 'A Beautiful Apartment', 55000, 'month', 1, 22, 'gbagada', 0, 1, 1, 1, 2),
(23, 'Beautiful Apartment', 'A Beautiful Apartment', 190000, 'month', 1, 23, 'lekki', 0, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `spacetype`
--

CREATE TABLE `spacetype` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacetype`
--

INSERT INTO `spacetype` (`id`, `name`) VALUES
(1, 'single space'),
(2, 'multi space');

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `profilepic` varchar(200) DEFAULT NULL,
  `occupation` varchar(100) DEFAULT NULL,
  `gender` varchar(30) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `paystackid` int(11) DEFAULT NULL,
  `remember_token` varchar(150) DEFAULT NULL,
  `access_token` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `provider` varchar(200) DEFAULT NULL,
  `provider_id` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `email`, `firstname`, `middlename`, `lastname`, `mobile`, `profilepic`, `occupation`, `gender`, `dob`, `password`, `paystackid`, `remember_token`, `access_token`, `name`, `provider`, `provider_id`) VALUES
(15, 'olaitan.adetayo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'GlWn6v5SYJhnsIwNH68FouHwG5DuO9yWpM8fvDG5jvKyd2GqFtlUodI79mH6', 'ya29.Gl1NBd4KsypxJkOWSaZjBVcjjBuR-Fz-QNFpj7Xw3ycYmN0IMwyWCYDoIMfIPYsAt_9FxEekwTHBqR3EjX8w8qCVJvJXxzUFDt4eTYURmu8DV41wnab8PGeEmzK1VhU', 'olaitan adetayo', 'google', '100570029115294211751');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amenities`
--
ALTER TABLE `amenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `availablelocations`
--
ALTER TABLE `availablelocations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeowners`
--
ALTER TABLE `homeowners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `potentialhomeowners`
--
ALTER TABLE `potentialhomeowners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spaceamenities`
--
ALTER TABLE `spaceamenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacebookingtransactions`
--
ALTER TABLE `spacebookingtransactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacecontracts`
--
ALTER TABLE `spacecontracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacegalleries`
--
ALTER TABLE `spacegalleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spaceinterests`
--
ALTER TABLE `spaceinterests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spaces`
--
ALTER TABLE `spaces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spacetype`
--
ALTER TABLE `spacetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amenities`
--
ALTER TABLE `amenities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `availablelocations`
--
ALTER TABLE `availablelocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `homeowners`
--
ALTER TABLE `homeowners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `potentialhomeowners`
--
ALTER TABLE `potentialhomeowners`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spaceamenities`
--
ALTER TABLE `spaceamenities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `spacebookingtransactions`
--
ALTER TABLE `spacebookingtransactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spacecontracts`
--
ALTER TABLE `spacecontracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spacegalleries`
--
ALTER TABLE `spacegalleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `spaceinterests`
--
ALTER TABLE `spaceinterests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spaces`
--
ALTER TABLE `spaces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `spacetype`
--
ALTER TABLE `spacetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
